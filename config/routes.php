<?php 

use \Bootie\App as App;
use \Controller\HomeController as HomeController;
use \Model\Post as Post;

/* public */
App::route('/',					['uses' => 'Controller\HomeController@index']);
App::route('/blog',				['uses' => 'Controller\BlogController@index']);
App::route('/search',			['uses' => 'Controller\BlogController@search']);
App::route('/blog/files/(\d+)', ['uses' => 'Controller\BlogController@files']);
App::route('/tag/([^/]+)', 		['uses' => 'Controller\BlogController@tag']);
App::route('/login', 			['uses' => 'Controller\AuthController@check_login']);
App::route('/login', 			['uses' => 'Controller\AuthController@login','method' => 'post']);
App::route('/contact', 			['uses' => 'Controller\HomeController@contact','method' => 'post']);
App::route('/updatepassword', 	['uses' => 'Controller\AuthController@update_password','method' => 'post','before' => 'auth.admin']);
App::route('/createpassword', 	['uses' => 'Controller\AuthController@create_password','method' => 'post']);
App::route('/logout', 			['uses' => 'Controller\AuthController@logout']);

/* private */
App::route('/admin', 			['uses' => 'Controller\AdminController@index','before' => 'auth.admin']);
App::route('/notifications', 	['uses' => 'Controller\HomeController@notifications','before' => 'auth.any']);
App::group('/admin/posts', 		['uses' => 'Controller\PostController','before' => 'auth.admin']);
App::group('/admin/words', 		['uses' => 'Controller\WordController','before' => 'auth.admin']);
App::group('/admin/projects', 	['uses' => 'Controller\ProjectController','before' => 'auth.admin']);
App::group('/admin/tasks', 		['uses' => 'Controller\TaskController','before' => 'auth.admin']);
App::group('/admin/milestones', ['uses' => 'Controller\MilestoneController','before' => 'auth.admin']);
App::group('/admin/schedules', 	['uses' => 'Controller\ScheduleController','before' => 'auth.admin']);
App::group('/admin/currencies', ['uses' => 'Controller\CurrencyController','before' => 'auth.admin']);
App::group('/admin/countries', 	['uses' => 'Controller\CountryController','before' => 'auth.admin']);
App::group('/admin/payments', 	['uses' => 'Controller\PaymentController','before' => 'auth.admin']);
App::group('/admin/users', 		['uses' => 'Controller\UserController','before' => 'auth.admin']);
App::route('/admin/calendar', 	['uses' => 'Controller\ScheduleController@show_calendar','before' => 'auth.admin']);
App::route('/admin/schedules/calendar', ['uses' => 'Controller\ScheduleController@calendar','before' => 'auth.admin']);

App::route('/account', 			['uses' => 'Controller\AccountController@index','before' => 'auth.account']);
App::group('/account/projects', ['uses' => 'Controller\Account\ProjectController','before' => 'auth.account']);
App::group('/account/schedules', ['uses' => 'Controller\Account\ScheduleController','before' => 'auth.account']);
App::route('/account/payments/info', ['uses' => 'Controller\AccountController@info','before' => 'auth.account']);
App::route('/account/payments/project/(\d+)', ['uses' => 'Controller\AccountController@project','before' => 'auth.account']);
App::group('/account/payments', ['uses' => 'Controller\Account\PaymentController','before' => 'auth.account']);
App::group('/account/tasks', 	['uses' => 'Controller\Account\TaskController','before' => 'auth.account']);
App::group('/account/milestones', ['uses' => 'Controller\Account\MilestoneController','before' => 'auth.account']);
App::route('/account/projects/(\d+)/milestones', ['uses' => 'Controller\Account\ProjectController@filter','before' => 'auth.account']);
App::route('/account/schedules/calendar/json', ['uses' => 'Controller\Account\ScheduleController@calendar','before' => 'auth.account']);

App::route('/admin/tags/relation/remove/(\d+)', ['uses' => 'Controller\TagController@remove_relation','method' => 'post','before' => 'auth.admin']);
App::route('/admin/tags/relation/add/(\d+)', ['uses' => 'Controller\TagController@add_relation','before' => 'auth.admin','method' => 'post']);
App::route('/admin/tags/post/(\d+)', ['uses' => 'Controller\TagController@tags','before' => 'auth.admin']);
App::route('/admin/files/resize', ['uses' => 'Controller\FileController@resize','method' => 'post','before' => 'auth.admin']);
App::route('/file/upload', ['uses' => 'Controller\FileController@upload','method' => 'post','before' => 'auth.admin']);
App::route('/admin/files/order', ['uses' => 'Controller\FileController@order','method' => 'post','before' => 'auth.admin']);
App::route('/admin/files/remove', ['uses' => 'Controller\FileController@destroy','method' => 'post','before' => 'auth.admin']);

App::route('/([^/]+)', 			['uses' => 'Controller\BlogController@show']);

/* public pages */
App::route('/(.*)', 			['uses' => 'Controller\HomeController@page']);

/* all views shared */

App::share('latest',Post::fetch([],6,0,['updated' => 'desc']));

/* filters */
App::filter('auth.any',function(){
	if( ! session('user_id')){
		die(redirect('/login',['Your session has expired']));
	}
});

App::filter('auth.admin',function(){
	if( ! session('user_id') OR session('role') !== 'admin'){
		die(redirect('/login',['Your session has expired']));
	}
});

App::filter('auth.account',function(){
	if( ! session('user_id') OR session('role') !== 'account'){
		die(redirect('/login',['Your session has expired']));
	}
});