<?php namespace Controller;

class ProjectController extends \Controller\BaseController {
	
	static $layout = "admin";

	/*
	public function index(){

		$entries = \Model\Project::paginate([
			'updated' => "DESC"
		],null,10);

		return \Bootie\App::view('admin.projects.index',[
			'entries'	=> $entries
		]);
	}

	public function create(){

		$p = new \Model\Project();
		$p->{'title_' . LOCALE} = 'New Project';
		$p->user_id = session('user_id');
		$p->created = TIME;
		$p->updated = TIME;
		$p->save();
		$id = $p->id;

		$entry = \Model\Project::row([
			'id' => $id
		]);
		
		return \Bootie\App::view('admin.projects.edit',[
			'entry'	=> $entry
		]);
	}

	public function edit($id){

		if(is_numeric($id))
		{
			$entry = \Model\Project::row([
				'id' => $id
			]);
			
			return \Bootie\App::view('admin.projects.edit',[
				'entry'	=> $entry
			]);
		}
		
		return \Exception('Project ID was not provided');
	}

	public function update($id){

		if(is_numeric($id))
		{

			extract($_POST);

			$entry = new \Model\Project();
			$entry->id = $id;
			$entry->slug = ! empty($slug) ? $slug : sanitize(${'title_' . LOCALE},false);
			$entry->updated = TIME;

			foreach(config()->languages as $lang){
				$entry->{'title_' . $lang} = ${'title_' . $lang};
				$entry->{'caption_' . $lang} = ${'caption_' . $lang};
				$entry->{'content_' . $lang} = ${'content_' . $lang};
			}

			$result = $entry->save();

			return redirect('/admin/projects',[
				'success' => "Entry <strong>{$entry->{'title_' . $lang}}</strong> has been updated"
			]);
		}

		return redirect('/admin/projects',[
			'danger' => "Entry ID was not provided"
		]);
	}

	public function delete($id){

		if(is_numeric($id))
		{
			$entry = \Model\Project::row([
				'id' => $id
			]);

			if( $entry )
			{

				$tags = \Model\ProjectTag::row([
					'project_id' => $id
				]);

				if($tags)
				{
					$tags->delete();
				}

				foreach($entry->files() as $file)
				{
					\Bootie\Image::destroy_group($file->name,'projects');

					\Model\File::row([
						'id' => $file->id
					])->delete();
				}

				$title = $entry->{'title_' . LOCALE};
				$entry->delete();

				return redirect('/admin/projects',[
					'success' => "Entry <strong>{$title}</strong> has been deleted"
				]);
			}
		}

		return redirect('/admin/projects',[
			'danger' => "Entry was not found"
		]);
	}*/
}