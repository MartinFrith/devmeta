<?php namespace Controller;

class BaseController {
	
	public function __construct(){
		\Bootie\App::load_database();
	}

	public function index(){
		$def = self::get_definition();
		$entries = $def->namespace::paginate([
			'updated' => "DESC"
		],null,10);

		return \Bootie\App::view($def->path . '.index',[
			'entries'	=> $entries
		]);
	}

	public function create(){

		$def = self::get_definition();

		$options = (object)[];
		$related = [$def->namespace::$belongs_to];
		$related = array_values(array_filter($related));

		foreach($related as $rel){
			foreach($rel as $key => $value){
				$options->{$key} = call_user_func_array(array($value,'fetch'),array());

				//$options->{$key} = call_user_func_array(array($value,'select'),array('fetch','*',NULL,['user_id' => session('user_id')]));
			}
		}

		return \Bootie\App::view($def->path,[
			'options' => $options
		]);
	}


	public function edit($id){

		if(is_numeric($id))
		{
			$def = self::get_definition();

			$entry = $def->namespace::row([
				'id' => $id
			]);

			$options = (object)[];
			$related = [$def->namespace::$belongs_to];
			$related = array_values(array_filter($related));

			foreach($related as $rel){
				foreach($rel as $key => $value){
					$options->{$key} = call_user_func_array(array($value,'fetch'),array());
				}
			}

			return \Bootie\App::view(preg_replace('/(\d)+$/','edit',$def->path) ,[
				'entry'	=> $entry,
				'options' => $options
			]);
		}
		
		return \Exception('Project ID was not provided');

	}
 
	public function update($id){

		$input = (object) $_POST;

		$def = self::get_definition();
		$db = new \Bootie\Database;
		$class = new \ReflectionClass($def->namespace);
		$table = $class->getStaticPropertyValue('table');
		$fields = $db->fetch('SHOW COLUMNS FROM ' . $table,null,'Field');
		$entry = new $def->namespace();

		if(!empty($id)) {
			$entry->id = $def->id;
			$input->created = TIME;
			$input->updated = TIME;
		}

		foreach($fields as $field){
			if(isset($input->{$field})){
				$value = $input->{$field};

				if( (bool) strtotime($value)){

					$type = $db->column('DESCRIBE ' . $table . ' ' . $field,null,1);

					if($type == 'int(14)'){
						$d = \DateTime::createFromFormat(config()->dateformat, $value);
						$value = $d->getTimestamp();
					}
				}

				$entry->{$field} = $value;
			}
		}

		if(!empty($fields->updated)){
			$entry->updated = TIME;
		}

		if($entry->save()) {

			if(empty($def->id)){
				return redirect( $def->index_url . '/' . $entry->id,[
					'success' => locale('row_successfully_updated')
				]);
			}

			return redirect($def->index_url,[
				'success' => locale('row_successfully_updated')
			]);

		} else {

			return redirect($def->index_url,[
				'success' => locale('row_not_updated')
			]);
		}

		return redirect($def->index_url,[
			'danger' => "Entry ID was not provided"
		]);
	}

	public function delete($id){

		$response["status"] = "warning";
		$response["message"] = "No ID was provided";

		if(is_numeric($id)){

			$def = self::get_definition();
			$entry = $def->namespace::find($id);

			if($entry->delete()) {
				$response["status"] = "success";
				$response["message"] = $def->class . " deleted";
			} else {
				$response["status"] = "error";
				$response["message"] = "Error when deleting " . $def->class;
			}

			return redirect($def->index_url,[
				'success' => locale('row_deleted')
			]);
		}
		
		return \Bootie\App::json($response);
	}	

	private static function get_definition(){

		

		$segments = array_filter(array_values(segments()));

		array_splice($segments,2);

		$index_url = '/' . implode('/',$segments);
		$reflection = new \ReflectionClass(get_called_class());
		$class = $reflection->getShortName();
		$definition = str_replace('Controller','',$class);
		$section = segments(2);
		$action = segments(3);

		$titleable = config()->titleable;

		if(!empty($section) AND !empty($action) AND in_array($action,['update','delete']))
		{
			$record = "";
			foreach($titleable as $title)
			{
				if(!empty($_REQUEST[$title])){
					$record = $_REQUEST[$title];
					break;
				}
				else if(!empty($_REQUEST[$title.'_'.LOCALE]))
				{
					$record = $_REQUEST[$title.'_'.LOCALE];
					break;
				}
			}

			$row = new \Model\History();
			$row->table = segments(2);
			$row->action = segments(3);
			$row->record = $record;
			$row->record_id = segments(4);
			$row->user_id = session('user_id');
			$row->created = TIME;
			$row->updated = TIME;
			$row->save();
		}
		
		return (object) [
			'namespace' => '\\Model\\' . $definition,
			'class'	=> $definition,
			'path' => implode('.',array_filter(array_values(segments()))),
			'id' => segments(count(segments())-1),
			'index_url' => $index_url
		];
	}
}