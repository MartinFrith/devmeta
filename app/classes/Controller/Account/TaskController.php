<?php namespace Controller\Account;

class TaskController extends \Controller\BaseController {
	
	static $layout = "account";

	public function index(){

		$db = new \Bootie\Database;

		$cells = $db->fetch('SELECT milestones.id  
			FROM milestones 
			LEFT JOIN projects ON projects.id = milestones.project_id
			WHERE projects.user_id = ' . session('user_id'));

		foreach($cells as $cell){
			$ids[]= $cell->id;
		}

		$entries = \Model\Task::paginate([
			'updated' => "DESC"
		],[
			'milestone_id IN(' . implode(',',$ids) . ')'
		],10);

		return \Bootie\App::view('account.tasks.index',[
			'entries'	=> $entries
		]);
	}
}