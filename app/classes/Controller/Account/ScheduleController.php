<?php namespace Controller\Account;

class ScheduleController extends \Controller\BaseController {
	
	static $layout = "account";



	public function calendar(){

		extract($_REQUEST);
		$d1 = \DateTime::createFromFormat('Y-m-d', $start);
		$d2 = \DateTime::createFromFormat('Y-m-d', $end);

		$start = $d1->getTimestamp();
		$end = $d2->getTimestamp();

		$db = new \Bootie\Database;

		$cells = $db->fetch('SELECT schedules.id  
			FROM schedules 
			LEFT JOIN tasks ON tasks.id = schedules.task_id
			LEFT JOIN milestones ON milestones.id = tasks.milestone_id
			LEFT JOIN projects ON projects.id = milestones.project_id
			WHERE projects.user_id = ' . session('user_id'));

		foreach($cells as $cell){
			$ids[]= $cell->id;
		}

		$schedules = \Model\Schedule::fetch([
			'id IN(' . implode(',',$ids) . ')'
			//'started < ' . $start,
			//'started > ' . $end
		]);

		$events = [];
		foreach($schedules as $schedule){
			if($schedule->task->milestone->project->user_id == session('user_id')){
				$span = timespan($schedule->started,$schedule->ended);
				$events[] = [
					'title' => $schedule->task->task . ' ('.$span.')',
					'url' => '/account/schedules/' . $schedule->id,
					'start' => date('c',$schedule->started),
					'end' => date('c',$schedule->ended),
				];
			}
		}

		return \Bootie\App::json($events);
	}
}