<?php namespace Controller\Account;

class ProjectController extends \Controller\BaseController {
	
	static $layout = "account";

	public function index(){

		$entries = \Model\Project::paginate([
			'updated' => "DESC"
		],[
			'user_id' => session('user_id')
		],10);

		return \Bootie\App::view('account.projects.index',[
			'entries'	=> $entries
		]);
	}

	public function filter($id){


		$entries = \Model\Project::paginate([
			'updated' => "DESC"
		],[
			'id' => $id
		],10);

		return \Bootie\App::view('account.milestones.index',[
			'entries'	=> $entries
		]);
	}

	private function is_owned($id){
		$db = new \Bootie\Database;
		$cell = $db->fetch('SELECT count(*) as count 
			FROM milestones 
			LEFT JOIN projects ON projects.id = milestones.project_id
			WHERE projects.user_id = ' . session('user_id') . ' 
			AND milestones.id = ' . $id);

		return $cell[0]->count;
	}

}