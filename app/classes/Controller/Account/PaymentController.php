<?php namespace Controller\Account;

use \Model\Project;
use \Model\Payment;

class PaymentController extends \Controller\BaseController {
	
	static $layout = "account";

	public function index(){

		$project_ids = Project::select('fetch','id',null,[
			'user_id' => session('user_id')
		]);

		$entries = Payment::paginate([
			'transferred' => "DESC"
		],[
			'project_id IN(' . implode(',',$project_ids) . ')'
		],10);

		return \Bootie\App::view('account.payments.index',[
			'entries'	=> $entries
		]);
	}
}