<?php namespace Controller;

class MilestoneController extends \Controller\BaseController {
	
	static $layout = "admin";

	public function index(){
		
		$entries = \Model\Project::paginate([
			'updated' => "DESC"
		],null,10);

		return \Bootie\App::view('admin.milestones.index',[
			'entries'	=> $entries
		]);

	}
}