<?php namespace Controller;

class WordController extends \Controller\BaseController {
	
	static $layout = "admin";

	public function delete($id){

		if(is_numeric($id))
		{
			$entry = \Model\Word::row([
				'id' => $id
			]);

			if( $entry )
			{

				$title = $entry->word_key;
				$entry->delete();

				return redirect('/admin/words',[
					'success' => "Entry <strong>{$title}</strong> has been deleted"
				]);
			}
		}

		return redirect('/admin/words',[
			'danger' => "Entry was not found"
		]);
	}
}