<?php namespace Controller;

class AdminController extends \Controller\BaseController {
	
	static $layout = "admin";

	public function index(){
		return \Bootie\App::view('admin.dash',[
			'posts_count'	=> \Model\Post::count(),
			'projects_count'	=> \Model\Project::count(),
			'tasks_count'	=> \Model\Task::count(),
			'milestones_count'	=> \Model\Milestone::count(),
			'schedule_count'	=> \Model\Schedule::count(),
			'payments_count'	=> \Model\Payment::count(),
			'words_count'	=> \Model\Word::count(),
			'users_count'	=> \Model\User::count(),
		]);
	}
}