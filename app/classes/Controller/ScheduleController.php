<?php namespace Controller;

class ScheduleController extends \Controller\BaseController {
	
	static $layout = "admin";

	public function show_calendar(){
		return \Bootie\App::view('admin.schedules.calendar');
	}

	public function calendar(){

		extract($_REQUEST);

		$d1 = \DateTime::createFromFormat('Y-m-d', $start);
		$d2 = \DateTime::createFromFormat('Y-m-d', $end);

		$start = $d1->getTimestamp();
		$end = $d2->getTimestamp();

		$schedules = \Model\Schedule::fetch([
			//'started < ' . $start,
			//'started > ' . $end
		]);

		$events = [];
		foreach($schedules as $schedule){
			$span = timespan($schedule->started,$schedule->ended);
			$events[] = [
				'title' => $span.'-' . $schedule->task->task,
				'url' => '/admin/schedules/' . $schedule->id,
				'start' => date('c',$schedule->started),
				'end' => date('c',$schedule->ended),
			];
		}

		return \Bootie\App::json($events);
	}
}