<?php namespace Controller;

class HomeController extends \Controller\BaseController {
	
	static $layout = "default";
		
	public function index(){
		$index = rand(0,4);
		$tag_arr = ['works','featured'];
		$fw = \Controller\BlogController::find_by_tag($tag_arr);
		$featured_tags = [];
		$featured_works = [];

		foreach($fw as $w){
			$i = 0;
			foreach($w->tags() as $tag){
				if($i) continue;
				if( ! empty($tag->tag) AND ! in_array($tag->tag,$tag_arr)){
					$featured_tags[$tag->tag][] = (object) $w;

					$i = 1;
				}
			}
		}

		return \Bootie\App::view('index',[
			'entry' => \Model\Post::row(['slug' => 'homepage']),
			//'posts' => \Model\Post::fetch([],6,0,['updated' => 'desc']),
			'works'	=> \Controller\BlogController::find_by_tag('works'),
			'featured_tags'	=> (object) $featured_tags,
			'bg' => '/images/bg/' . HomeController::getRandomHeaderImage(1)[0],
			'title' => locale('index_title_' . $index),
			'caption' => locale('index_caption_' . $index)
		]);
	}

	public function notifications(){

		$rows = \Model\History::fetch([
			'user_id' => session('user_id')
		],5,1,[
			'created' => 'desc'
		]);

		$response = [];

		foreach($rows as $row){
			$response[] = [
				'icon' => !empty(\Model\History::$icons[$row->action]) ? \Model\History::$icons[$row->action] : 'ion-question',
				'table' => locale($row->table),
				'action' => locale($row->action),
				'record' => $row->record,
				'timespan' => timespan($row->created)
			];
		}

		return \Bootie\App::json($response);
	}


	public function contact(){

		$post = (object) $_POST;
		$message = implode('<br>',[$post->name,$post->email,$post->message]);

		\Bootie\Mail\Mailer::send('hello@devmeta.net','Nuevo contacto desde devmeta.net',[
			'message' => $message
		]);

		return \Bootie\App::view("static.contact-recieved");
	}

	public function page($slug){
		return \Bootie\App::view("static.$slug",[
			'title' => "",
			'caption' => "",
			'bg' => "",
		]);
	}

	public function getRandomHeaderImage($size=0){
		$available = [];

		$extensions = array('jpg', 'jpeg', 'png', 'gif');

		// init result
		$result = array();

		// directory to scan
		$directory = new \DirectoryIterator(dirname(__FILE__) . '/../../../public/images/bg/');

		// iterate
		foreach ($directory as $fileinfo) {
		    // must be a file
		    if ($fileinfo->isFile()) {
		        // file extension
		        $extension = strtolower(pathinfo($fileinfo->getFilename(), PATHINFO_EXTENSION));
		        // check if extension match
		        if (in_array($extension, $extensions)) {
		            // add to result
		            $result[] = $fileinfo->getFilename();
		        }
		    }
		}

		shuffle($result);
		array_splice($result, $size);
		
		return $result;
	}
}