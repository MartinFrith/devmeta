<?php namespace Controller;

use \Model\Task;
use \Model\Project;
use \Model\Schedule;
use \Model\Milestone;

class AccountController extends \Controller\BaseController {
	
	static $layout = "account";

	public function index(){

		$db = new \Bootie\Database;
		$balance = 0;

		$cell = $db->fetch('SELECT count(*) as count 
			FROM schedules  
			LEFT JOIN tasks ON tasks.id = schedules.task_id 
			LEFT JOIN milestones ON milestones.id = tasks.milestone_id 
			LEFT JOIN projects ON projects.id =  milestones.project_id
			WHERE projects.user_id = ' . session('user_id'));

		$schedule_count = $cell[0]->count;

		$cell = $db->fetch('SELECT count(*) as count 
			FROM tasks 
			LEFT JOIN milestones ON milestones.id = tasks.milestone_id 
			LEFT JOIN projects ON projects.id =  milestones.project_id
			WHERE projects.user_id = ' . session('user_id'));

		$task_count = $cell[0]->count;

		$cell = $db->fetch('SELECT count(*) as count 
			FROM milestones 
			LEFT JOIN projects ON projects.id = milestones.project_id
			WHERE projects.user_id = ' . session('user_id'));

		$milestone_count = $cell[0]->count;

		$project_ids = Project::select('fetch','id',null,[
			'user_id' => session('user_id')
		]);

		$payments = $db->column('SELECT SUM(amount) as total  
			FROM payments 
			WHERE project_id IN(' . implode(',',$project_ids) . ')',null,0);

		$milestone_ids = Milestone::select('fetch','id',null,[
			'project_id IN(' . implode(',',$project_ids) . ')'
		]);

		$task_ids = Task::select('fetch','id',null,[
			'milestone_id IN(' . implode(',',$milestone_ids) . ')'
		]);

		$schedules = Schedule::fetch([
			'task_id IN(' . implode(',',$task_ids) . ')'
		]);

		foreach($schedules as $schedule){
			$hours= ($schedule->ended - $schedule->started)/3600;
			$balance+=$hours * $schedule->task->milestone->project->hour_rate;
		}

		return \Bootie\App::view('account.dash',[
			'project_count' => \Model\Project::count(['user_id' => session('user_id')]),
			'schedule_count' => $schedule_count,
			'task_count' => $task_count,
			'milestone_count' => $milestone_count,
			'balance' => $payments - $balance
		]);
	}

	public function project($id){


		$project = Project::row([
			'id' => $id,
			'user_id' => session('user_id')
		]);


		if(!$project)
			return \Bootie\App::view('errors.404');

		$entry = [
			'title' => $project->title,
			'currency' => $project->currency->code,
		];

		$db = new \Bootie\Database;

		$payments = $db->column('SELECT SUM(amount) as total  
			FROM payments 
			WHERE project_id IN(' . $id . ')',null,0);


		$milestone_ids = Milestone::select('fetch','id',null,[
			'project_id IN(' . $id . ')'
		]);

		$task_ids = Task::select('fetch','id',null,[
			'milestone_id IN(' . implode(',',$milestone_ids) . ')'
		]);

		$schedules = Schedule::fetch([
			'task_id IN(' . implode(',',$task_ids) . ')'
		]);

		foreach($schedules as $schedule){
			$hour = ($schedule->ended - $schedule->started)/3600;
			$id = $schedule->task->milestone->project->id;
			if(empty($entry['balance']))
				$entry['balance'] = 0;

			$entry['balance']+=$hour * $schedule->task->milestone->project->hour_rate;
		}			

		$balance = 0;
		$subtotal = 0;
		$total_hours = 0;
		$data = [];
		foreach($schedules as $schedule){
			$id = $schedule->task->milestone->project->id;
			$hour = ($schedule->ended - $schedule->started)/3600;
			$invoice = $hour * $schedule->task->milestone->project->hour_rate;
			$subtotal+=$invoice;
			$total_hours+=$hour;

			$data[]= [
				'hours' 		=> $hour,
				'task' 			=> $schedule->task->task,
				'date' 			=> $schedule->started,
				'invoice' 		=> $invoice,
				'subtotal' 		=> $subtotal,
				'caption' 		=> $schedule->caption
			];
		}	

		$entry['data'] = $data;
		$entry['total_hours'] = $total_hours;
		$entry['subtotal'] = $subtotal;
		$entry['payments'] = $payments;
		$entry['balance'] = $payments - $subtotal;

		return \Bootie\App::view('account.payments.project',[
			'entry'	=> $entry
		]);
	}

	public function info(){
		return \Bootie\App::view('account.payments.info');
	}	
}