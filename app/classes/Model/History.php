<?php namespace Model;

class History extends \Bootie\ORM { 
    public static $table = 'history';
    public static $icons = [
    	'delete' => "ion-android-cancel",
    	'update' => "ion-checkmark-circled",
    	'login' => "ion-log-in",
    	'logout' => "ion-log-out"
    ];

    public static $belongs_to = array(
        'user' => '\Model\User',
    );  	
}