<?php namespace Model;

class Milestone extends \Bootie\ORM { 
    public static $table = 'milestones';
  	public static $foreign_key = 'milestone_id';

    public static $belongs_to = array(
        'project' => '\Model\Project',
    );  	

    public static $has = array(
        'tasks' => '\Model\Task',
    );  	
}