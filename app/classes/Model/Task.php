<?php namespace Model;

class Task extends \Bootie\ORM { 
    public static $table = 'tasks';
  	public static $foreign_key = 'task_id';

    public static $belongs_to = array(
        'milestone' => '\Model\Milestone',
    );  	

    public static $has = array(
        'schedules' => '\Model\Schedule',
    );  	
}