<?php namespace Model;

class Payment extends \Bootie\ORM { 
    public static $table = 'payments';
  	public static $foreign_key = 'payment_id';

    public static $belongs_to = array(
        'project' => '\Model\Project',
        'currency' => '\Model\Currency',
    );    
}