<?php namespace Model;
 
class Word extends \Bootie\ORM
{
    public static $table = 'words';
    public static $awesome_icons = [];

    public static $meta = [
		'animations' => [
			"" => "Sin animación",
			"faa-shake animated" => "Sacudir",
			"faa-pulse animated" => "Pulsar",
			"faa-float animated" => "Flotar",
			"faa-spin animated" => "Girar",
			"faa-bounce animated" => "Rebotar",
			"faa-flash animated" => "Fogonazo",
			"faa-horizontal animated" => "Horizontal",
			"faa-vertical animated" => "Vertical",
			"faa-ring animated" => "Anillo",
			"faa-wrench animated" => "Llave",
			"faa-shake animated-hover" => "Sacudir (ratón encima)",
			"faa-pulse animated-hover" => "Pulsar (ratón encima)",
			"faa-float animated-hover" => "Flotar (ratón encima)",
			"faa-spin animated-hover" => "Girar (ratón encima)",
			"faa-bounce animated-hover" => "Rebotar (ratón encima)",
			"faa-flash animated-hover" => "Fogonazo (ratón encima)",
			"faa-horizontal animated-hover" => "Horizontal (ratón encima)",
			"faa-vertical animated-hover" => "Vertical (ratón encima)",
			"faa-ring animated-hover" => "Anillo (ratón encima)",
			"faa-wrench animated-hover" => "Llave (ratón encima)"
		],
		'colors' => [
			"" => "Sin color",
			"blue" => "Azul",
			"red" => "Rojo",
			"green" => "Verde",
			"orange" => "Naranja",
			"grey" => "Gris",
			"white" => "Blanco"
		]
	];
}