<?php namespace Model;

class Currency extends \Bootie\ORM { 
    public static $table = 'currencies';
  	public static $foreign_key = 'currency_id';

}