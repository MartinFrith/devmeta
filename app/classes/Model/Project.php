<?php namespace Model;

class Project extends \Bootie\ORM { 
    public static $table = 'projects';
  	public static $foreign_key = 'project_id';

    public static $belongs_to = array(
        'user' => '\Model\User',
        'currency' => '\Model\Currency',
    );    

    public static $has = array(
        'milestones' => '\Model\Milestone',
    );    
}