<?php namespace Model;

class Schedule extends \Bootie\ORM { 
    public static $table = 'schedules';
  	public static $foreign_key = 'schedule_id';

    public static $belongs_to = array(
        'task' => '\Model\Task',
    );    
}