<?php namespace Model;

class Country extends \Bootie\ORM { 
    public static $table = 'countries';
  	public static $foreign_key = 'country_id';

    public static $belongs_to = array(
        'currency' => '\Model\Currency'
    );  	
}