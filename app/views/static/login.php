
            <!-- blog-info -->
            <div class="blog-info" id="blog">
                <!-- -start-img-cursual- -->
                <div class="wrap">
                <!-- start content_slider -->
                    <div id="owl-demo2" class="owl-carousel2">
                        <div class="item">
                            <div class="item-center">

                                <form class="form ajax form-controls get-intouch-center-form" action="/login">
                                    <h2>Login</h2>
                                    <div class="form-group">
                                        <input type="text" name="email" placeholder="Email or username">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" placeholder="**********">
                                    </div>
                                    <?php echo messages();?>
                                    <div class="alert alert-danger hide"></div>
                                    <div class="col-sm-12 text-right">
                                        <span class="submit descriptionColor">
                                            <input type="submit" value="<?php echo locale('start-session');?>" class="titleColor" />
                                        </span>
                                        <div id="contactResponse" class="text-left"></div>
                                    </div>                      
                                </form>
                            </div>
                            <div class="clear"> </div>
                        </div>
                    </div>
                </div>
            </div>                                

        <script type="text/javascript">

            $('form').submit(function(e){

                e.preventDefault();

                $.ajax({
                  type: 'post',
                  url: 'login',
                  data: $(this).serialize(),
                  success : function(json){
                    if(json.redirect){
                      location.href = json.redirect;
                    }
                    if( ! json.result){
                      $('.alert-danger')
                        .html("Username and/or password are incorrect")
                        .removeClass("hide")
                        .hide()
                        .slideDown();
                    }
                  }
                });

                return false;
                
            }); 

        </script>