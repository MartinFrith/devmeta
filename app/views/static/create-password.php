<div class="section section-we-made-3" id="projects">
    <div class="container">
        <div class="row">
            <div class="title add-animation">
                <h2><?php echo locale('login');?></h2>
                <div class="separator-container">
                    <div class="separator line-separator"><span class="typcn typcn-star-full-outline"></span></div>
                </div>
                <p><?php echo locale('login-reset-pass');?><br><br></p>
				<form class="form ajax form-controls" action="/createpassword">
					<input type="hidden" name="loginoremail" value="<?php echo $_GET['loginoremail'];?>">
					<input type="hidden" name="login" value="1">
					<div class="form-group">
						<input type="password" name="password" class="form-control input-lg" placeholder="<?php echo locale('password');?>">
					</div>
					<div class="form-group">
						<input type="password" name="password2" class="form-control input-lg" placeholder="<?php echo locale('repeat-password');?>">
					</div>
					<?php echo messages();?>
					<div class="alert alert-danger hide"></div>
					<div class="form-group">
						<button type="submit" class="btn btn-lg btn-success btn-login"><span class="typcn typcn-brush"></i> <?php echo locale('create-password');?></button>
					</div>
				</form>
            </div>
        </div>
    </div>
</div>
