<div class="section section-we-made-3" id="projects">
    <div class="container">
        <div class="row">
            <div class="title add-animation">
                <h2><?php echo locale('error');?></h2>
                <div class="separator-container">
                    <div class="separator line-separator"><span class="typcn typcn-star-full-outline"></span></div>
                </div>
                <p><?php echo locale('error-title');?><br><br></p>
            </div>
        </div>
    </div>
</div>
