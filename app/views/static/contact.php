<div class="section section-blue section-headed">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h3><span class="typcn typcn-flash"></span> <?php echo locale('contact-title');?></h3>
                <hr/>
                <div class="form">
                    <!-- Contact form -->
                    <form name="form_contact" action="/contact" method="post" class="form-horizontal">
                        <!-- Name -->
                        <div class="control-group">
                            <div class="controls">
                                <input type="text" class="form-control" name="full_name" minlength="2" data-validation-minlength-message="<?php echo locale('char-length-min-2');?>" placeholder="<?php echo locale('contact-your-name');?>" required />
                            </div>
                        </div>
                        <!-- Email -->
                        <div class="control-group">
                            <div class="controls">
                                <input type="email" class="form-control" name="email" data-validation-email-message="<?php echo locale('contact-check-email');?>" placeholder="<?php echo locale('contact-your-email');?>" required />
                            </div>
                        </div>
                        <!-- Telephone -->
                        <div class="control-group">
                            <div class="controls">
                                <input type="text" class="form-control" name="skype" minlength="2" data-validation-minlength-message="<?php echo locale('char-length-min-2');?>" placeholder="<?php echo locale('contact-your-skype');?>" required />
                            </div>
                        </div>
                        <!-- Address -->
                        <div class="control-group">
                            <div class="controls">
                                <textarea class="form-control" name="comment" rows="5" minlength="10" data-validation-minlength-message="<?php echo locale('char-length-min-10');?>" placeholder="<?php echo locale('contact-your-comment');?>" required /></textarea>
                            </div>
                        </div>
                        <div class="control-group col-sm-offset-1">
                            <div class="controls">
                                <label class="checkbox hand">
                                    <input type="checkbox" name="terms" data-validation-required-message="Debes aceptar las condiciones de uso y política de privacidad" required />
                                    <?php echo locale('terms-accept');?> <a href="javascript:void(0);" onclick="pop_at('ajax/tos');"><?php echo locale('use-terms');?></a> <?php echo locale('and');?> <a href="javascript:void(0);" onclick="pop_at('ajax/legal');"><?php echo locale('privacy-policy');?></a>
                                </label>
                            </div>
                        </div>
                        <div class="group-control">&nbsp;</div>
                        <!-- Buttons -->
                        <div class="form-actions pull-right">
                            <button type="submit" class="btn btn-lg btn-neutral"><i class="fa fa-paper-plane"></i><?php echo locale('contact-send');?></button>
                        </div>
                        <div class="group-control">&nbsp;</div>
                        <div class="group-control">&nbsp;</div>
                        <div class="group-control">&nbsp;</div>
                    </form>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="group-control">&nbsp;</div>     
                <div class="alert alert-info">
                    <p><?php echo locale('contact-text');?><p>
                    <hr>
                    <h3>Devmeta IT Software <a href="skype:devmeta.net?call" title="Skype"><span class="typcn typcn-social-skype"></span></a></h3>

                    <strong><?php echo locale('address');?></strong> <span>C/Cámpora, 636 9200 Esquel </span><br>
                    <strong><?php echo locale('phone');?></strong> <span>+54 915 504 687</span><br>
                    <strong><?php echo locale('email');?></strong> <span><a href="mailto:hello@devmeta.net">hello@devmeta.net</a></span>
                    <div id="map_geo"></div>                
                    <hr />
                    <div>
                        <!-- Social media icons -->
                        <strong><?php echo locale('wanna-hear-from-us');?></strong>
                        <div class="social">
                            
                            <a href="https://www.twitter.com/devmeta_net" title="Twitter" target="_blank"><span class="typcn typcn-social-twitter"></span></i></a>
                            <a href="http://blogs.devmeta.net" title="Blogs" target="_blank"><span class="typcn typcn-chat"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    

<style type="text/css">
  #map_geo { height: 200px; }
</style>

