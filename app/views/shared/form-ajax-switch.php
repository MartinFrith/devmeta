<script>

    $(function(){
        $('.onoffswitch').click(function() {
            var $this = $(this);
            if($this.attr('updating')==0){
                $this.attr('updating',1);
                var state = $this.find('input').is(':checked')?1:0;
                $.ajax({
                    type:       "POST",
                    cache:      false, 
                    url:       '/<?php echo PATH;?>' + '/' + $this.data('name') + '/' + state,
                    async:      true,
                    data : { _token : window.Laravel.csrfToken}
                }).done(function(json){
                    $this.attr('updating',0);
                });            
            }
        });        
    });

</script>