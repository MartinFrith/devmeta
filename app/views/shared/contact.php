
        <!--Get in touch section-->
        <section id="sContact" class="obliqueCut lightSection graySection separatorTop separatorBottom">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="separator" viewBox="0 0 100 100" preserveAspectRatio="none">
                <path d="M0 100 L100 0 L100 100 Z"></path>
            </svg>
            <div class="sectionIcon icon_pin_alt"></div>
            <a name="contact"></a>
            <div class="container">
                <!--SECTION TITLE-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="sectionInfo row">
                            <h2 class="sectionTitle col-sm-4"><span class="underline"><?php print locale('contact');?></span></h2>
                            <h2 class="sectionDescription col-sm-8"><span class="titleColor"></span></h2>
                        </div>
                    </div>
                </div>
                <!--END SECTION TITLE-->

                <div class="row">
                    <!--SOCIAL CONTACT-->
                    <div class="col-sm-3">
                        <ul class="socialList style2" data-appear="fade-in">
                            <li><a class="soc_skype" href="skype:devmeta.net?chat">Call us</a></li>
                            <li><a class="soc_github" href="https://github.com/devmeta">Fork us</a></li>
                            <li><a class="soc_twitter" href="https://twitter.com/devmeta_net">Follow us</a></li>
                        </ul>
                    </div>
                    <!--END SOCIAL CONTACT-->

                    <!--CONTACT FORM-->

                    <div class="col-sm-8 col-sm-offset-1">
                        <form method="post" class="contactForm" action="/contact" data-appear="fade-in" data-appear-delay="300">
                            <h2>Say hello!</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="text" name="name" placeholder="Name:" />
                                </div>
                                <div class="col-sm-6">
                                    <input type="email" name="email" placeholder="Email:" />
                                </div>
                                <div class="col-sm-12">
                                    <textarea name="message" placeholder="Message:" rows="1"></textarea>
                                </div>
                                <div class="col-sm-12 text-right">
                                    <span class="submit descriptionColor">
                                        <input type="submit" value="<?php echo locale('contact-send');?>" class="titleColor" />
                                    </span>
                                    <div id="contactResponse" class="text-left"></div>
                                </div>
                            </div>
                            <div class="contactBusy"><div class="progressContainer"><span class="typing_loader"></span></div>
                            </div>
                        </form>
                    </div>
                    <!--END CONTACT FORM-->
                </div>
            </div>
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="separator" viewBox="0 0 100 100" preserveAspectRatio="none">
                <path d="M0 100 L100 0 L0 0 Z"></path>
            </svg>
        </section>
        <!--END Get in touch section-->

        <!--Google Map-->
        <section class="obliqueCut lightSection separatorTop paddingTop0 paddingBottom0">
            <div class="gmap_container" data-appear="fade-in">
                <div id="gmap_canvas">
                    <!--ENTER ONLY HERE THE OFFICE LOCATION-->
                    <span id="ourLocationX">-42.898164</span>
                    <span id="ourLocationY">-71.3001613</span>
                    <div id="visitUsContainer">
                        <input type="text" required placeholder="Visit us from..." id="startLocation" class="animate" onkeydown="if (event.keyCode == 13) calcRoute()" />
                        <span class="icon_close animate" onclick="removeRoute()"></span>
                    </div>
                    <!--CUSTOMIZE HERE INFO BALOON-->
                    <div id="gMapPopup">
                        <h2 class='underline'><b>Devmeta</b></h2>
                        <span>Our office</span>
                    </div>
                </div>
            </div>
        </section>
        <!--END Google Map-->