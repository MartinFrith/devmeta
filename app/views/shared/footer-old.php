
        <!--Footer-->
        <!--footer id="footer" class="obliqueCut lightSection separatorTop">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="separator" viewBox="0 0 100 100" preserveAspectRatio="none">
                <path d="M0 100 L100 0 L100 100 Z"></path>
            </svg>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 text-left">
                        <div><span class="highlight">&copy; <a href="https://bitbucket.org/MartinFrith/devmeta" target="_blank">Devmeta</a></span> All rights Reserved</div>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div><a href="/login" class="highlight"><?php echo locale('login');?></a></div>

                    </div>
                <div class="pull-right">
                    <a target="_blank" title="<?php print locale('twitter');?>" data-placement="bottom" href="https://twitter.com/devmeta_net">
                        <span class="typcn typcn-social-twitter"></span>
                    </a>
                    <a target="_blank" title="<?php print locale('github');?>" data-placement="bottom" href="https://github.com/devmeta">
                        <span class="typcn typcn-social-github"></span>
                    </a>
                    <a target="_blank" title="<?php print locale('skype');?>" data-placement="bottom" href="skype:devmeta.net?chat">
                        <span class="typcn typcn-social-skype"></span>
                    </a>
                    <a target="_blank" title="<?php print locale('mail');?>" data-placement="bottom" href="mailto:martin@devmeta.net">
                        <span class="typcn typcn-mail"></span>
                    </a>
                </div>                    
                </div>
            </div>
        </footer-->
        