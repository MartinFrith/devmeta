                    <div class="wrap"> 
                    <!-- logo -->
                    <div class="logo">
                        <h1><a href="/">dev.meta<span> </span> </a> </h1>
                    </div>
                    <div class="bc-entry">
                        <div class="bc-entry-prefix">><span class="bc-entry-fancy"></span><span class="blinking-cursor">|</span></div>
                        <input type="text" class="bc-entry-text" name="bc-entry" autocomplete="off" />
                    </div>
                    <div class="bc-results"></div>
                                        <!---//logo--->
                    <!--top-nav---->
                    <div class="top-nav">
                        <div class="nav-icon">
                        <a href="#" class="right_bt" id="activator"><span> </span> </a>
                        </div>
                         <div class="box" id="box">
                             <div class="box_content">                                                   
                                <div class="box_content_center">
                                    <div class="form_content">
                                        <div class="menu_box_list">
                                            <ul>
                                                <li><a href="<?php echo empty(segments(1))?'':'/';?>#home"><span><?php echo locale('home');?></span></a></li>
                                                <li><a href="<?php echo empty(segments(1))?'':'/';?>#about"><span><?php echo locale('about');?></span></a></li>
                                                <li class="<?php echo (segments(1) == 'tag' && segments(2) == 'works' ? ' active' : '');?>"><a href="/tag/works"><span><?php print locale('portfolio');?></span></a></li>
                                                <li class="<?php echo (segments(1) == 'blog' ? ' active' : '');?>"><a href="/blog"><span><?php echo locale('blog');?></span></a></li>
                                                <li><a href="<?php echo empty(segments(1))?'':'/';?>#contact"><span><?php echo locale('contact');?></span></a></li>
                                                <div class="clear"> </div>
                                            </ul>
                                        </div>
                                        <div class="menu_box_list menu_box_list--dotted">
                                            <ul>
                                            <?php foreach(config()->languages as $lang):?>
                                                <li class="group-list-item"><a href="?lang=<?php print $lang;?>"><?php print $lang == LOCALE ? '<span class="typcn typcn-tick-outline"></span>':'';?> <?php print locale($lang);?></a></li>
                                            <?php endforeach;?>
                                            </ul>
                                        </div>
                                        <a class="boxclose" id="boxclose"> <span> </span></a>
                                    </div>                                  
                                </div>  
                            </div> 
                        </div>            
                    </div>
                    <!--start-click-drop-down-menu-->
                    <!--start-dropdown-->
                     <script type="text/javascript">
                        var height = 0;
                        var $ = jQuery.noConflict();
                            $(function() {
                                $('#activator').click(function(){
                                    height = $(window).height();
                                    $('.box_content_center').css({'height':height+'px'});
                                    $('#box').animate({'top':'0px'},700);
                                });
                                
                                $('#boxclose').click(function(){
                                    $('#box').animate({'top':'-'+height+'px'},700);
                                });
                            });
                            $(document).ready(function(){
                            //Hide (Collapse) the toggle containers on load
                            $(".toggle_container").hide(); 
                            //Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
                            $(".trigger").click(function(){
                                $(this).toggleClass("active").next().slideToggle("slow");
                                    return false; //Prevent the browser jump to the link anchor
                            });
                                                
                        });
                    </script>
                    <!---//End-click-drop-down-menu----->
                    <!--top-nav---->
                    <div class="clear"> </div>
                    <!-- header-welcomenote -->
                <?php if( ! empty($title) AND ! empty($caption)):?>
                    <div class="header-welcome-note">
                        <h2><?php print $caption;?></h2>
                        <a href="#about"><span> </span> <?php print $title;?></a>
                    </div>
                <?php endif;?>
                    <!---//header-welcomenote--->
                </div>
