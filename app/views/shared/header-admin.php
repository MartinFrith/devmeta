<nav class="navbar navbar-inverse navbar-fixed-top admin" role="navigation">
    <ul class="top-notifications"></ul> 
	<div class="container">
		<div class="navbar-header pull-left">
			<a href="/admin"><span class="ion ion-leaf"></span> Devmeta</a>
		</div>

        <div class="collapse navbar-collapse exp-collapse">
            <div class="nav navbar-nav navbar-right navbar-uppercase navbar-select">
                <div class="pull-right">
                    <div class="dropdown">
            	        <div class="dropdown">
            	            <button class="btn btn-lang dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><span class="typcn typcn-world"></span></button>
            	            <ul class="dropdown-menu nav-pills" aria-labelledby="dropdownMenu1">
            	            <?php foreach(config()->languages as $lang):?>
            	                <li class="group-list-item"><a href="#" class="js-search" data-toggle="link" data-search="lang=<?php print $lang;?>"><?php print $lang == LOCALE ? '<span class="typcn typcn-tick-outline"></span>':'';?> <?php print locale($lang);?></a></li>
            	            <?php endforeach;?>
            	            </ul>
            	        </div>
                    </div>
                </div>
                <div class="pull-right">
                <?php if(session('user_id')):?>
                    
                    <!--a href="/settings" title="<?php print locale('settings');?>" data-placement="bottom">
                        <span class="typcn typcn-spanner"></span>
                    </a-->
                    <a href="/logout" title="<?php print locale('logout');?>" data-placement="bottom">
                        <span><span class="typcn typcn-user"></span> <?php echo session('title');?></span>
                        <span class="typcn typcn-key-outline"></span>
                    </a>
                <?php else:?>
                    <a href="/login" title="<?php print locale('login');?>" data-placement="left">
                        <span class="typcn typcn-key"></span>
                    </a>
                <?php endif;?>   
                </div>
            </div> 
        </div>        
	</div>
</nav>
