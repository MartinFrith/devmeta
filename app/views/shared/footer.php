
            <!-- start-getintouch- -->
            <div class="get-intouch" id="contact">
                <div class="wrap">
                    <h3><?php print locale('get-in-touch');?><span> </span></h3>
                    <div class="get-intouch-grids">
                        <div class="get-intouch-left-address">
                            <h4><?php print locale('get-in-touch-text');?></h4>
                            <p>9200 Costanera H. Cámpora, 636</p>
                            <p>Esquel, Argentina.</p>
                            <p>+54 9 2945 690 367</p>
                            <p><a href="mailto:hello@devmeta.net">hello@devmeta.net</a></p>
                        </div>
                        <div class="get-intouch-center-form">
                            <h5><?php print locale('say-hello');?></h5>
                            <div class="contact-response"></div>
                            <form id="form_contact">
                                <input type="text" name="name" value="" class="frist" placeholder="<?php print locale('your-name');?>" required>
                                <input type="email" name="email" value="" placeholder="<?php print locale('email');?>" required>
                                <textarea name="message" placeholder="<?php print locale('message');?>" required></textarea>
                                <input type="submit" value="<?php print locale('send-message');?>" />
                                <div class="clear"> </div>
                            </form>
                        </div>
                        <div class="get-intouch-right-social">
                            <ul>
                                <!--li><a class="be normalTip" title="behance" href="#"> </a></li-->
                                <li><a class="twitter normalTip" title="Twitter" href="https://twitter.com/devmeta_net" target="_blank"> </a></li>
                                <!--li><a class="dribble normalTip" title="dribble" href="#"> </a></li>
                                <li><a class="tree normalTip" title="treehouse" href="#"> </a></li>
                                <li><a class="google normalTip" title="google+" href="#"> </a></li-->
                            </ul>
                        </div>
                        <!-- aToolTip js -->
                            <script type="text/javascript" src="/js/jquery.atooltip.js"></script>
                            <script type="text/javascript">
                                $(function(){ 
                                    $('a.normalTip').aToolTip();
                                    }); 
                                    $('#form_contact').submit(function(){
                                        $.post('/contact',$(this).serialize(),function(resp){
                                            $('.contact-response').html(resp)
                                        })
                                        return false
                                    })
                            </script>
                        <div class="clear"> </div>
                    </div>
                </div>
                <div class="map" id="map"> </div>
                <link href='https://api.mapbox.com/mapbox.js/v2.4.0/mapbox.css' rel='stylesheet' />
                <script type="text/javascript" src='https://api.mapbox.com/mapbox.js/v2.4.0/mapbox.js'></script>
                <script src="/js/mapHelper.js"></script>
                <script type="text/javascript">
                    var lat = -42.898164
                    , lng = -71.3001613
                    , driverDetails = {
                        colorId : 1
                        , status : "waiting"
                        , displayName : "dev.meta"
                    }

                    L.mapbox.accessToken = H.mapbox.accessToken;
                    map = L.mapbox.map('map', 'mapbox.streets');
                    map.setView([lat,lng], 3);

                    //Display a default marker
                    marker = L.marker([lat,lng], {icon:H.icon(driverDetails)}).addTo(map);

                </script>
            </div>
            <!-- //End-getintouch- -->
            <!-- -start-copy-right -->
            <div class="copy-right">
                <div class="wrap">
                    <div class="copy-right-left">
                    <p>&#169; <?php echo date('Y');?> <span>dev.meta</span> All Rights Reserved.
                    </div>
                    <div class="copy-right-right">
                        <p>Template by <a href="http://w3layouts.com/">W3layouts</a> | <a href="/login"><?php echo locale('login');?></a></p>
                        <a href="#move-top" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"> </span></a>
                    </div>
                    <div class="clear"> </div>
                </div>
            </div>
            <!-- -//End-copy-right -->
            <!-- smoth-scrlling- -->
            <script type="text/javascript">

                var check_scroll = function (e) {

                    if($('#box').css('top') == '0px' && $(e.target).parents('#box').length){
                        $('#boxclose').click();
                    }
                    
                    var target = location.hash,
                    $target = $(target);

                    if(!$target.length) return null;

                    $('html, body').stop().animate({
                        'scrollTop': $target.offset().top
                    }, 1000, 'swing', function () {
                        window.location.hash = target;
                    });
                }

                $(document).ready(function(){
                    $('a[href^="#"]').on('click',function(){
                        e.preventDefault()
                        check_scroll()
                    })
                    $('.bc-entry-text').on('click keyup',function(){
                        $('.bc-entry-fancy').text($.trim($(this).val()))
                        if($(this).val().length<2) return $('.bc-results').slideUp()
                        location.hash = '/s/' + $('.bc-entry-text').val()
                    })
                    setTimeout(function(){
                        if(location.hash.length){
                          check_scroll()      
                        } 
                    },200)                    
                })
                
                $(window).on('hashchange', function(){
                    if(location.hash.substr(1,3)=='/s/'){
                        var key = location.hash.split('#')[1]
                        , key = location.hash.split('/')[2]
                        $.ajax({
                            method:'get',
                            url: '/search',
                            data : {key:key},
                            success : function(resp){
                                $('.bc-results').html('')
                                if( ! $(resp).length) $('.bc-results').slideUp(100)
                                
                                $(resp).each(function(i,item){
                                    $('.bc-results').append('<a href="'+item.slug+'"><div class="bc-result-row"><h1>'+item['title_'+locale]+'</h1><p>'+item['caption_'+locale]+'</p><small>'+item.created_readable+'</small><div class="background" style="background-image:url(/upload/posts/sd/'+item.name+')"></div></div></a>')
                                })
                                $('.bc-results').append('<div class="clearfix"></div>').slideDown()
                            }
                        })
                    }
                })

            </script>
        <!-- //smoth-scrlling- -->
        <!-- //End-content -->
        <!-- -//wrap- -->

