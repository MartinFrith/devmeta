<div class="content-height">
	<div class="center-block row">
		<div class="col-md-12">
			<div class="container">
				<h3>&nbsp;<span class="typcn typcn-device-desktop"></span></i> <?php print locale('dashboard');?> </h3 class="pull-right">
				<p><?php print locale('dashboard-account-text');?></p>
			</div>
			<div class="pills">
				<a class="col-lg-3 col-md-4 col-sm-6" href="/account/projects">
					<div class="panel panel-body panel-success">
						<h1><span class="typcn typcn-folder-open"></span></h1>
						<h3 class="pull-right"><?php echo locale('projects');?></h3 class="pull-right">
						<span class="badge"><?php echo $project_count;?></span>
					</div>
				</a>
				<a class="col-lg-3 col-md-4 col-sm-6" href="/account/tasks">
					<div class="panel panel-body panel-success">
						<h1><span class="typcn typcn-document-text"></span></h1>
						<h3 class="pull-right"><?php echo locale('tasks');?></h3 class="pull-right">
						<span class="badge"><?php echo $task_count;?></span>
					</div>
				</a>
				<a class="col-lg-3 col-md-4 col-sm-6" href="/account/milestones">
					<div class="panel panel-body panel-success">
						<h1><span class="typcn typcn-flag"></span></h1>
						<h3 class="pull-right"><?php echo locale('milestones');?></h3 class="pull-right">
						<span class="badge"><?php echo $milestone_count;?></span>
					</div>
				</a>

				<a class="col-lg-3 col-md-4 col-sm-6" href="/account/schedules">
					<div class="panel panel-body panel-success">
						<h1><span class="typcn typcn-stopwatch"></span></h1>
						<h3 class="pull-right"><?php echo locale('schedules');?></h3 class="pull-right">
						<span class="badge"><?php echo $schedule_count;?></span>
					</div>
				</a>
				<a class="col-lg-3 col-md-4 col-sm-6" href="/account/payments">
					<div class="panel panel-body panel-success">
						<h1><span class="ion ion-<?php echo $balance>=0?'happy':'sad';?>-outline"></span></h1>
						<h3 class="pull-right"><?php echo locale('balance');?></h3 class="pull-right">
						<span class="badge"><?php print session('currency');?> <?php echo $balance;?></span>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>

<?php include SP . 'app/views/admin/shared/notifications.php';?>