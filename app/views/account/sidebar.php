		<div class="group-control">&nbsp;</div>
		<div class="list-group">
			<a href="/account/projects" class="list-group-item<?php echo segments(2) == 'projects' ? ' active' : '';?>"><span class="typcn typcn-folder-open"></span> <?php print locale('projects');?> </a>
			<a href="/account/tasks" class="list-group-item<?php echo segments(2) == 'tasks' ? ' active' : '';?>"><span class="typcn typcn-document-text"></span> <?php print locale('tasks');?> </a>
			<a href="/account/milestones" class="list-group-item<?php echo segments(2) == 'milestones' ? ' active' : '';?>"><span class="typcn typcn-flag"></span> <?php print locale('milestones');?> </a>
			<a href="/account/schedules" class="list-group-item<?php echo segments(2) == 'schedules' ? ' active' : '';?>"><span class="typcn typcn-stopwatch"></span> <?php print locale('schedules');?> </a>
			<a href="/account/payments" class="list-group-item<?php echo segments(2) == 'payments' ? ' active' : '';?>"><span class="typcn typcn-credit-card"></span> <?php print locale('payments');?> </a>
		</div>
