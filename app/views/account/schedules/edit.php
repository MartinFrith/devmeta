<div class="content-height">
	<div class="center-block row">
		<div class="col-md-12">
			<h3>&nbsp;<span class="typcn typcn-world"></span> <?php echo $entry->task->task;?></h3>
			<input type="hidden" name="id" value="<?php echo $entry->id;?>">
			<form class="form" method="post" action="/admin/schedules/update/<?php echo $entry->id;?>">
				<div class="form-group">
					<select class="form-control " name="task_id" disabled>
					<?php foreach($options->task as $task):$task->milestone->load();?>
						<option value="<?php echo $task->id;?>"<?php echo $task->id == $entry->task_id?' selected':'';?>>[<?php echo !empty($task->milestone->project->data)?$task->milestone->project->data['title']:'?';?>] <?php echo $task->task;?></option>
					<?php endforeach;?>
					</select>
				</div>

				<div class="form-group">
					<input type="text" name="started" class="form-control datetimepicker " placeholder="started" value="<?php echo parse_date($entry->started);?>" disabled />
				</div>

				<div class="form-group">
					<input type="text" name="ended" class="form-control datetimepicker" placeholder="ended" value="<?php echo parse_date($entry->ended);?>" disabled />
				</div>

				<div class="form-group">
					<textarea class="form-control summernote" name="caption" placeholder="<?php print locale('caption');?>" disabled><?php echo $entry->caption;?></textarea>
				</div>
				
				<!--div class="form-actions form-group text-center">
					<button type="submit" class="btn btn-lg btn-success"> <span class="typcn typcn-tick-outline"></span> &nbsp;<?php print locale('save');?> </button>
				</div><div class="clearfix"></div-->
			</form>
		</div>
	</div>
</div>