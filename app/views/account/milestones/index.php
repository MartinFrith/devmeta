<div class="content-height">
	<div class="center-block row">
		<div class="col-sm-3 col-lg-2">
			<?php include SP . 'app/views/account/sidebar.php';?>
		</div>	
		<div class="col-sm-9 col-lg-10">
			<h3>&nbsp;<span class="typcn typcn-flag"></span> <?php print locale('milestones');?> <a href="/account/milestones/create" role="button" class="text-success" title="Create a new milestone" data-placement="right"><span class="typcn typcn-document-add"></span></a></h3>
			<?php echo messages();?>

		<?php if(count($entries)):?>
			<table class="table">
				<tr>
					<th width="20%"><?php print locale('project');?></th>
					<th><?php print locale('milestones');?></th>
					<th><?php print locale('tasks');?></th>
					<th><?php print locale('estimated');?></th>
					<th><?php print locale('done');?></th>
					<!--th class="text-right"><span class="typcn typcn-cog"></span></th-->
				</tr>
			<?php foreach($entries as $project):?>
				<tr>
					<td><span class="text-success"><span class="typcn typcn-folder-open"></span> <?php print $project->title;?></span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<?php foreach($project->milestones() as $milestone):?>
				<tr>
					<td>&nbsp;</td>
					<td><span class="text-success"><span class="typcn typcn-flag"></span> <a href="/account/milestones/<?php print $milestone->id;?>"><?php print $milestone->milestone;?></a></span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>

				</tr>
					<?php foreach($milestone->tasks() as $task):?>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><span class="text-success"><span class="typcn typcn-document-text"></span> <a href="/account/tasks/<?php print $task->id;?>"><?php print $task->task;?></a></span></td>
							<td><span class="text-success"><span class="typcn typcn-time"></span> <?php print $task->estimated;?>h</span></td>
							<td><span class="typcn typcn-<?php echo $task->done?'tick':'times';?>"></span></span></td>
						</tr>
					<?php endforeach;?>				
				<?php endforeach;?>
			<?php endforeach;?>
			</table>
			<?php $entries[0]->paginator();?>
		<?php endif;?>
		
		</div>
	</div>
</div>