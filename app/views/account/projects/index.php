<div class="content-height">
	<div class="center-block row">
		<div class="col-sm-3 col-lg-2">
			<?php include SP . 'app/views/account/sidebar.php';?>
		</div>		
		<div class="col-sm-9 col-lg-10">
			<div class="">
				<h3>&nbsp;<span class="typcn typcn-folder-open"></span> <?php print locale('projects');?> <a href="/account/projects/create" role="button" class="text-success" title="Create a new project" data-placement="right"><span class="typcn typcn-document-add"></span></a></h3>
				<?php echo messages();?>
			<?php if(count($entries)):?>
				<table class="table">
					<tr>
						<th><?php print locale('project');?></th>
						<th><?php print locale('milestones');?></th>
						<th><?php print locale('created');?></th>
					</tr>
				<?php foreach($entries as $project): ?>
					<tr>
						<td><a href="/account/projects/<?php echo $project->id;?>"><span class="text-success"><?php print $project->title;?></span></a></td>
						<td><a href="/account/projects/<?php echo $project->id;?>/milestones"><span class="typcn typcn-document-text"></span></a><span class="text-success"><?php print count($project->milestones());?></span></td>
						<td><span class="text-success"><?php print timespan($project->created);?></span></td>
					</tr>
				<?php endforeach;?>
				</table>
				<?php $entries[0]->paginator();?>
			<?php else:?>
					<p> <?php print locale('listing_is_empty');?></p>
			<?php endif;?>
			</div>
		</div>
	</div>
</div>