<div class="content-height">
	<div class="center-block row">
		<div class="col-sm-3 col-lg-2">
			<?php include SP . 'app/views/account/sidebar.php';?>
		</div>		
		<div class="col-sm-9 col-lg-10">
			<h3>&nbsp;<span class="typcn typcn-credit-card"></span> <?php echo $entry['title'];?></h3>
			<?php echo messages();?>

			<div class="list-group">
		<?php if(!empty($entry['data'])):?>
			<?php foreach($entry['data'] as $data):?>
				<div class="list-group-item">
					<dd>
						<strong><?php echo locale('task');?></strong> <span><a href="#" class="task"><?php echo $data['task'];?></a></span>
					</dd>
					<dd>
						<strong><?php echo locale('date');?></strong> <span><?php echo timespan($data['date']);?></span>
					</dd>
					<dd>
						<strong><?php echo locale('hours');?></strong> <span><?php echo $data['hours'];?> 
						<strong><?php echo locale('invoice');?></strong> <?php echo $entry['currency'];?> <?php echo $data['invoice'];?></span>
					</dd>
					<dd>
						<strong><?php echo locale('subtotal');?></strong> <span> <?php echo $entry['currency'];?> <?php echo $data['subtotal'];?> 
					</dd>
					<dd class="details hide">
						<strong><?php echo locale('caption');?></strong> <span><?php echo $data['caption'];?></span>
					</dd>
				</div>
			<?php endforeach;?>
				<div class="well">
					<dd>
						<strong><?php echo locale('hours');?></strong> <span><?php echo $entry['total_hours'];?></span>
					</dd>
					<dd>
						<strong><?php echo locale('subtotal');?></strong> <span><?php echo $entry['currency'];?> <?php echo $entry['subtotal'];?></span>
					</dd>
					<dd>
						<strong><?php echo locale('payments');?></strong> <span><?php echo $entry['currency'];?> <?php echo $entry['payments'];?></span>
					</dd>
					<dd>
						<strong><?php echo locale('balance');?></strong> <span><?php echo $entry['currency'];?> <?php echo $entry['balance'];?></span>
					</dd>

				</div>
		<?php else:?>
				<h5><?php echo locale('project_empty');?></h5>
		<?php endif;?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('.task').click(function(e){
			e.preventDefault();
			var div = $(this).parent().parent().parent().find('.details');
			if(div.hasClass('hide')) div.removeClass('hide');
			else div.toggle();
			return false;
		});
	})
</script>