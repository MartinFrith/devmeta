<div class="content-height">
	<div class="center-block row">
		<div class="col-md-12">
			<h3>&nbsp;<span class="typcn typcn-credit-card"></span> <?php echo locale('payment');?></h3>
			<form class="form" method="post" action="/account/payments/update/0">
				<input type="hidden" name="user_id" value="<?php echo session('user_id');?>">
				<div class="form-group">
					<input type="number" name="amount" class="form-control input-lg" placeholder="<?php echo locale('amount');?>" value="" />
				</div>
				<div class="form-group">
					<select class="form-control " name="currency_id">
					<?php foreach($options->currency as $currency):?>
						<option value="<?php echo $currency->id;?>"<?php echo $currency->id == session('currency_id')?' selected':'';?>><?php echo $currency->code;?></option>
					<?php endforeach;?>
					</select>
				</div>				
				<div class="form-actions form-group text-center">
					<button type="submit" class="btn btn-lg btn-success"> <span class="typcn typcn-tick-outline"></span> &nbsp;<?php print locale('save');?> </button>
				</div><div class="clearfix"></div>
			</form>
		</div>
	</div>
</div>