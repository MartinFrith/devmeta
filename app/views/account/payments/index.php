<div class="content-height">
	<div class="center-block row">
		<div class="col-sm-3 col-lg-2">
			<?php include SP . 'app/views/account/sidebar.php';?>
		</div>		
		<div class="col-sm-9 col-lg-10">
			<h3>&nbsp;<span class="typcn typcn-credit-card"></span> <?php print locale('payments');?>  
				<a href="/account/payments/create" role="button" class="text-success" title="New payment" data-placement="right">
					<span class="typcn typcn-document-add"></span>
				</a>
				<a href="/account/payments/info" class="text-info" title="Info" data-placement="right">
					<span class="typcn typcn-info"></span>
				</a>
			</h3>
			<?php echo messages();?>

		<?php if(count($entries)):?>
			<table class="table">
				<tr>
					<th><?php print locale('project');?></th>
					<th><?php print locale('amount');?></th>
					<th><?php print locale('transferred');?></th>
				</tr>
			<?php foreach($entries as $payment): ?>
				<tr>
					<td><a href="/account/payments/project/<?php echo $payment->project->id;?>"><?php print $payment->project->title;?></a></span></td>
					<td><a href="/account/payments/<?php echo $payment->id;?>"><span class="text-success"><?php print $payment->currency->code;?> <?php print $payment->amount;?> </span></a></td>					
					<td><span class="text-success"><?php print timespan($payment->transferred);?></span></td>
				</tr>
			<?php endforeach;?>
			</table>
			<?php $entries[0]->paginator();?>
		<?php endif;?>
		</div>
	</div>
</div>