<div class="content-height">
	<div class="center-block row">
		<div class="col-md-12">
			<h3>&nbsp;<span class="typcn typcn-credit-card"></span> <?php echo $entry->amount;?></h3>
			<input type="hidden" name="id" value="<?php echo $entry->id;?>">
			<form class="form" method="post" action="/account/payments/update/<?php echo $entry->id;?>">

				<div class="form-group">
					<input type="text" name="amount" class="form-control input-sm" placeholder="amount" value="<?php echo $entry->amount;?>" />
				</div>

				<div class="form-group">
					<select class="form-control " name="currency_id">
					<?php $cid = isset($entry->currency_id) ? $entry->currency_id : session('currency_id');?>
					<?php foreach($options->currency as $currency):?>
						<option value="<?php echo $currency->id;?>"<?php echo $currency->id == $cid?' selected':'';?>><?php echo $currency->code;?></option>
					<?php endforeach;?>
					</select>
				</div>

				<div class="form-group">
					<select class="form-control " name="project_id">
					<?php foreach($options->project as $project):?>
						<option value="<?php echo $project->id;?>"<?php echo $project->id == $entry->project_id?' selected':'';?>><?php echo $project->title;?></option>
					<?php endforeach;?>
					</select>
				</div>

				<div class="form-group">
					<input type="text" name="transferred" class="form-control datetimepicker " placeholder="transferred" value="<?php echo parse_date($entry->transferred);?>" />
				</div>

				<div class="form-group">
					<textarea class="form-control summernote" name="caption" placeholder="<?php print locale('caption');?>"><?php echo $entry->caption;?></textarea>
				</div>

				<div class="form-actions form-group text-center">
					<button type="submit" class="btn btn-lg btn-success"> <span class="typcn typcn-tick-outline"></span> &nbsp;<?php print locale('save');?> </button>
				</div><div class="clearfix"></div>
			</form>
		</div>
		<div class="col-md-3">
			<div class="group-control">&nbsp;</div>

		</div>
	</div>
</div>