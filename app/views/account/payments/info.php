<div class="content-height">
	<div class="center-block row">
		<div class="col-sm-3 col-lg-2">
			<?php include SP . 'app/views/account/sidebar.php';?>
		</div>		
		<div class="col-sm-9 col-lg-10">
			<h3>&nbsp;<span class="typcn typcn-credit-card"></span> <?php print locale('payments_info');?></h3>
			<hr>
			<h3><?php echo locale('payments_bank');?></h3>
			<table class="table table-striped">
				<tr>
					<td><?php echo locale('bank');?></td>
					<td><img src="/assets/img/logo-santander.gif"></td>
				</tr>
				<tr>
					<td><?php echo locale('account');?></td>
					<td>Cuenta Unica 168-360187/3</td>
				</tr>
				<tr>
					<td><?php echo locale('cbu');?></td>
					<td>0720168088000036018736</td>
				</tr>
			</table>

			<h3><?php echo locale('payments_money_orders');?></h3>
			<table class="table table-striped">
				<tr>
					<td><?php echo locale('first_name');?></td>
					<td>Martín Aníbal</td>
				</tr>
				<tr>
					<td><?php echo locale('last_name');?></td>
					<td>Frith</td>
				</tr>
				<tr>
					<td><?php echo locale('dni');?></td>
					<td>26315691</td>
				</tr>
				<tr>
					<td><?php echo locale('address');?></td>
					<td>Costanera H. Cámpora 636 depto. 4</td>
				</tr>
				<tr>
					<td><?php echo locale('city');?></td>
					<td>Esquel (9200)</td>
				</tr>
				<tr>
					<td><?php echo locale('country');?></td>
					<td>Argentina</td>
				</tr>

			</table>

		</div>
	</div>
</div>