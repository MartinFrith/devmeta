<div class="content-height">
	<div class="center-block row">
		<div class="col-md-12">
			<h3>&nbsp;<span class="typcn typcn-spanner-outline"></span> <?php echo locale('task');?></h3>
			<form class="form" method="post" action="/account/tasks/update/0">
				<div class="form-group">
					<input type="text" name="task" class="form-control input-lg" placeholder="<?php echo locale('task');?>" value="" />
				</div>

				<div class="form-group">
					<select class="form-control" name="project_id">
					<?php foreach($options->project as $project):?>
						<option value="<?php echo $project->id;?>"><?php echo $project->title;?></option>
					<?php endforeach;?>
					</select>
				</div>

				<div class="form-actions form-group text-center">
					<button type="submit" class="btn btn-lg btn-success"> <span class="typcn typcn-tick-outline"></span> &nbsp;<?php print locale('save');?> </button>
				</div><div class="clearfix"></div>
			</form>
		</div>
	</div>
</div>