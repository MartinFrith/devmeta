<div class="content-height">
	<div class="center-block row">
		<div class="col-sm-3 col-lg-2">
			<?php include SP . 'app/views/account/sidebar.php';?>
		</div>	
		<div class="col-sm-9 col-lg-10">
			<div class="">
				<h3>&nbsp;<span class="typcn typcn-document-text"></span> <?php print locale('tasks');?> <a href="/account/tasks/create" role="button" class="text-success" title="Create a new task" data-placement="right"><span class="typcn typcn-document-add"></span></a></h3>
				<?php echo messages();?>
			<?php if(count($entries)):?>
				<table class="table">
					<tr>
						<th><?php print locale('task');?></th>
						<th><?php print locale('estimated');?></th>
						<th><?php print locale('done');?></th>
						<!--th class="text-right"><span class="typcn typcn-cog"></span></th-->
					</tr>
				<?php foreach($entries as $task):?>
					<tr>
						<td><span class="text-success"><span class="typcn typcn-folder-open"></span> <?php print $task->task;?></span></td>
						<td><?php print $task->estimated;?>h</td>
						<td><span class="typcn typcn-<?php echo $task->done?'tick':'times';?>"></span></span></td>
					</tr>
				<?php endforeach;?>
				</table>
				<?php $entries[0]->paginator();?>
			<?php endif;?>
			</div>
		</div>
	</div>
</div>