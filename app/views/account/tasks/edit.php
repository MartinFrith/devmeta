<div class="content-height">
	<div class="center-block row">
		<div class="col-md-12">
			<h3>&nbsp;<span class="typcn typcn-world"></span> <?php echo $entry->task;?></h3>
			<input type="hidden" name="id" value="<?php echo $entry->id;?>">
			<form class="form" method="post" action="/account/tasks/update/<?php echo $entry->id;?>">
				<div class="form-group">
					<input type="text" name="task" class="form-control input-sm" placeholder="task" value="<?php echo $entry->task;?>" />
				</div>
				<div class="form-group">
					<select class="form-control" name="milestone_id">
					<?php foreach($options->milestone as $milestone):?>
						<option value="<?php echo $milestone->id;?>"<?php echo $milestone->id == $entry->milestone_id?' selected':'';?>><?php echo $milestone->milestone;?></option>
					<?php endforeach;?>
					</select>
				</div>
				<div class="form-group">
					<textarea class="form-control summernote" name="caption" placeholder="<?php print locale('caption');?>"><?php echo $entry->caption;?></textarea>
				</div><div class="clearfix"></div>

			<?php if($entry->schedules()):?>
				<h3><span class="typcn typcn-time"></span> <?php echo locale('schedules');?></h3>
				<table class="table table-striped">
					<tr>
						<th><?php print locale('started');?></th>
						<th><?php print locale('time');?></th>
						<th><?php print locale('caption');?></th>
					</tr>
				<?php foreach($entry->schedules() as $schedule):?>
					<tr>
						<td><?php print date(config()->dateformat,$schedule->started);?></td>
						<td><?php print timespan($schedule->started,$schedule->ended);?></td>
						<td><?php print $schedule->caption;?></td>
					</tr>
				<?php endforeach;?>
				</table>
			<?php endif;?>

				<div class="form-actions form-group text-center">
					<button type="submit" class="btn btn-lg btn-success"> <span class="typcn typcn-tick-outline"></span> &nbsp;<?php print locale('save');?> </button>
				</div><div class="clearfix"></div>
			</form>
		</div>
		<div class="col-md-3">
			<div class="group-control">&nbsp;</div>

		</div>
	</div>
</div>