<script id="notifications" type="text/x-jsrender">
	<li><span class="ion {{:icon}}"></span> &nbsp;<span>{{:table}}</span> <span>{{:action}}</span> <span>{{:record}}</span> <i>{{:timespan}}</i></li>
</script>

<script type="text/javascript">
	
	$.get('notifications',function(json){
		var tpl = $.templates("#notifications")
		$('.top-notifications').html(tpl.render(json));
		var clock = undefined;
		clock = setInterval(function(){
			var row = $('.top-notifications').find('li:visible').first();
			row.fadeTo('slow',0, function(){
				row.hide().next().fadeTo('slow',1, function(){
					$(this).show();
				});
			});
			if(!row.length){
				$('.top-notifications').hide();
				clearInterval(clock);
			} 

		},3000);
	});

</script>