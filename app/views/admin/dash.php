<div class="content-height">
	<div class="center-block row">
		<div class="col-md-12">
			<div class="container">
				<h1>&nbsp;<span class="typcn typcn-device-desktop"></span></i> <?php print locale('dashboard');?> </h1>
				<p><?php print locale('dashboard-admin-text');?></p>
			</div>
			<div class="pills">
				<a class="col-lg-3 col-md-4 col-sm-6" href="/admin/posts">
					<div class="panel panel-body panel-success">
						<h1><span class="typcn typcn-edit"></span></h1>
						<h3 class="pull-right"><?php echo locale('posts');?></h3 class="pull-right">
						<span class="badge"><?php echo $posts_count;?></span>
					</div>
				</a>
				<a class="col-lg-3 col-md-4 col-sm-6" href="/admin/projects">
					<div class="panel panel-body panel-success">
						<h1><span class="typcn typcn-folder-open"></span></h1>
						<h3 class="pull-right"><?php echo locale('projects');?></h3 class="pull-right">
						<span class="badge"><?php echo $projects_count;?></span>
					</div>
				</a>

				<a class="col-lg-3 col-md-4 col-sm-6" href="/admin/milestones">
					<div class="panel panel-body panel-success">
						<h1><span class="typcn typcn-flag"></span></h1>
						<h3 class="pull-right"><?php echo locale('milestones');?></h3 class="pull-right">
						<span class="badge"><?php echo $milestones_count;?></span>
					</div>
				</a>

				<a class="col-lg-3 col-md-4 col-sm-6" href="/admin/tasks">
					<div class="panel panel-body panel-success">
						<h1><span class="typcn typcn-document-text"></span></h1>
						<h3 class="pull-right"><?php echo locale('tasks');?></h3 class="pull-right">
						<span class="badge"><?php echo $tasks_count;?></span>
					</div>
				</a>
				
				<a class="col-lg-3 col-md-4 col-sm-6" href="/admin/schedules">
					<div class="panel panel-body panel-success">
						<h1><span class="typcn typcn-stopwatch"></span></h1>
						<h3 class="pull-right"><?php echo locale('schedules');?></h3 class="pull-right">
						<span class="badge"><?php echo $schedule_count;?></span>
					</div>
				</a>

				<a class="col-lg-3 col-md-4 col-sm-6" href="/admin/calendar">
					<div class="panel panel-body panel-success">
						<h1><span class="typcn typcn-calendar"></span></h1>
						<h3 class="pull-right"><?php echo locale('calendar');?></h3 class="pull-right">
						<span class="badge"><?php echo $schedule_count;?></span>
					</div>
				</a>

				<a class="col-lg-3 col-md-4 col-sm-6" href="/admin/payments">
					<div class="panel panel-body panel-success">
						<h1><span class="typcn typcn-credit-card"></span></h1>
						<h3 class="pull-right"><?php echo locale('payments');?></h3 class="pull-right">
						<span class="badge"><?php echo $payments_count;?></span>
					</div>
				</a>
				<a class="col-lg-3 col-md-4 col-sm-6" href="/admin/users">
					<div class="panel panel-body panel-success">
						<h1><span class="typcn typcn-user"></span></h1>
						<h3 class="pull-right"><?php echo locale('users');?></h3 class="pull-right">
						<span class="badge"><?php echo $users_count;?></span>
					</div>
				</a>																
				<a class="col-lg-3 col-md-4 col-sm-6" href="/admin/words">
					<div class="panel panel-body panel-success">
						<h1><span class="typcn typcn-sort-alphabetically"></span></h1>
						<h3 class="pull-right"><?php echo locale('words');?></h3 class="pull-right">
						<span class="badge"><?php echo $words_count;?></span>
					</div>
				</a>																
			</div>
		</div>
	</div>
</div>

<?php include SP . 'app/views/admin/shared/notifications.php';?>