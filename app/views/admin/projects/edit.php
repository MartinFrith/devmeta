<div class="content-height">
	<div class="center-block row">
		<div class="col-md-12">
			<h3>&nbsp;<span class="typcn typcn-world"></span> <?php echo $entry->title;?></h3>
			<input type="hidden" name="id" value="<?php echo $entry->id;?>">
			<form class="form" method="post" action="/admin/projects/update/<?php echo $entry->id;?>">
				<div class="form-group">
					<input type="text" name="title" class="form-control input-sm" placeholder="title" value="<?php echo $entry->title;?>" />
				</div>
				<div class="form-group">
					<select class="form-control" name="user_id">
					<?php foreach($options->user as $user):?>
						<option value="<?php echo $user->id;?>"<?php echo $user->id == $entry->user_id?' selected':'';?>><?php echo $user->title;?></option>
					<?php endforeach;?>
					</select>
				</div>
				<div class="form-group">
					<select class="form-control" name="currency_id">
					<?php foreach($options->currency as $currency):?>
						<option value="<?php echo $currency->id;?>"<?php echo $currency->id == $entry->currency_id?' selected':'';?>><?php echo $currency->name;?></option>
					<?php endforeach;?>
					</select>
				</div>
				<div class="form-group">
					<input type="text" name="hour_rate" class="form-control input-sm" placeholder="hour_rate" value="<?php echo $entry->hour_rate;?>" />
				</div>				
				<div class="form-group">
					<input type="text" name="website" class="form-control input-sm" placeholder="website" value="<?php echo $entry->website;?>" />
				</div>
				<div class="form-group">
					<textarea class="form-control summernote" name="caption" placeholder="<?php print locale('caption');?>"><?php echo $entry->caption;?></textarea>
				</div><div class="clearfix"></div>

				<div class="form-actions form-group text-center">
					<button type="submit" class="btn btn-lg btn-success"> <span class="typcn typcn-tick-outline"></span> &nbsp;<?php print locale('save');?> </button>
				</div><div class="clearfix"></div>
			</form>
		</div>
	</div>
</div>