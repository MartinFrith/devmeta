<div class="content-height">
	<div class="center-block row">
		<div class="col-sm-3 col-lg-2">
			<?php include SP . 'app/views/admin/sidebar.php';?>
		</div>	
		<div class="col-sm-9 col-lg-10">
			<h3>&nbsp;<span class="typcn typcn-world"></span> <?php print locale('projects');?> <a href="/admin/projects/create" role="button" class="text-success" title="Create a new project" data-placement="right"><span class="typcn typcn-document-add"></span></a></h3>
			<?php echo messages();?>
		<?php if(count($entries)):?>
			<table class="table">
				<tr>
					<th><?php print locale('project');?></th>
					<th><?php print locale('client');?></th>
					<th class="text-right"><span class="typcn typcn-cog"></span></th>
				</tr>
			<?php foreach($entries as $project):?>
				<tr>
					<td><span><?php print $project->title;?></span></td>
					<td><span class="text-success"><?php echo count($project->user->data) ? $project->user->title  : '';?></span></td>
					<td class="text-right">
						<form class="nowrap" action="/admin/projects/delete/<?php echo $project->id;?>" method="post">
						<button type="submit" class="btn btn-danger" onclick="if(!confirm('Your are about to delete this project, are you sure?')) return false;" title="Delete"><span class="typcn typcn-times-outline"></span></button>
						<a href="/admin/projects/<?php echo $project->id;?>" class="btn btn-success" title="Edit"><span class="typcn typcn-edit"></span></a>
						</form>
					</td>
				</tr>
			<?php endforeach;?>
			</table>
			<?php $entries[0]->paginator();?>
		<?php else:?>
				<p> Words is empty! You may create one.</p>
		<?php endif;?>
			<p> <a href="/" target="_blank">Go to Frontpage</a></p>
		</div>
	
	</div>
</div>