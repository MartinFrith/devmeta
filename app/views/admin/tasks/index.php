<?php include SP . 'app/views/shared/form-ajax-switch.php';?>

<div class="content-height">
	<div class="center-block row">
		<div class="col-sm-3 col-lg-2">
			<?php include SP . 'app/views/admin/sidebar.php';?>
		</div>	
		<div class="col-sm-9 col-lg-10">
			<h3>&nbsp;<span class="typcn typcn-spanner-outline"></span> <?php print locale('tasks');?> <a href="/admin/tasks/create" role="button" class="text-success" title="Create a new task" data-placement="right"><span class="typcn typcn-document-add"></span></a></h3>
			<?php echo messages();?>
		<?php if(count($entries)):?>
			<table class="table">
				<tr>
					<th><?php print locale('task');?></th>
					<th><?php print locale('estimated');?></th>
					<th><?php print locale('done');?></th>
					<th class="text-right"><span class="typcn typcn-cog"></span></th>
				</tr>
			<?php foreach($entries as $task):?>
				<tr>
					<td><span class="text-success"><span class="typcn typcn-folder-open"></span> <?php print $task->task;?></span></td>
					<td><?php print $task->estimated;?>h</td>
					<td>
	                    <div class="onoffswitch" data-userid="{{ $entry->id }}" data-permissionid="{{ $permission->id }}" updating="0" title="<?php echo locale('done');?>">
	                    	<input type="hidden" name="done" value="<?php echo $task->done;?>">
	                        <input type="checkbox" class="onoffswitch-checkbox" id="myonoffswitch"<?php echo $task->done?' checked':'';?>>
	                        <label class="onoffswitch-label" for="myonoffswitch">
	                            <span class="onoffswitch-inner"></span>
	                            <span class="onoffswitch-switch"></span>
	                        </label>
	                    </div>
					</td>
					<td class="text-right">
						<form class="nowrap" action="/admin/tasks/delete/<?php echo $task->id;?>" method="post">
						<button type="submit" class="btn btn-danger" onclick="if(!confirm('Your are about to delete this task, are you sure?')) return false;" title="Delete"><span class="typcn typcn-times-outline"></span></button>
						<a href="/admin/tasks/<?php echo $task->id;?>" class="btn btn-success" title="Edit"><span class="typcn typcn-edit"></span></a>
						</form>
					</td>						
				</tr>
			<?php endforeach;?>
			</table>
			<?php $entries[0]->paginator();?>
		<?php endif;?>
		</div>
	
	</div>
</div>