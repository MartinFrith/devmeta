<div class="content-height">
	<div class="center-block row">
		<div class="col-md-12">
			<h3>&nbsp;<span class="typcn typcn-world"></span> <?php echo $entry->task;?></h3>
			<input type="hidden" name="id" value="<?php echo $entry->id;?>">
			<form class="form" method="post" action="/admin/tasks/update/<?php echo $entry->id;?>">
				<div class="form-group">
                    <div class="onoffswitch" title="<?php echo locale('done');?>">
                    	<input type="hidden" name="done" value="<?php echo $entry->done;?>">
                        <input type="checkbox" class="onoffswitch-checkbox" id="myonoffswitch"<?php echo $entry->done?' checked':'';?>>
                        <label class="onoffswitch-label" for="myonoffswitch">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>

				<div class="form-group">
					<input type="text" name="task" class="form-control input-sm" placeholder="task" value="<?php echo $entry->task;?>" />
				</div>
				<div class="form-group">
					<input type="number" name="estimated" class="form-control input-sm" placeholder="task" value="<?php echo $entry->estimated;?>" />
				</div>				
				<div class="form-group">
					<select class="form-control" name="milestone_id">
					<?php foreach($options->milestone as $milestone):?>
						<option value="<?php echo $milestone->id;?>"<?php echo $milestone->id == $entry->milestone_id?' selected':'';?>>[<?php echo $milestone->project->title;?>] <?php echo $milestone->milestone;?></option>
					<?php endforeach;?>
					</select>
				</div>
				<div class="form-group">
					<textarea class="form-control summernote" name="caption" placeholder="<?php print locale('caption');?>"><?php echo $entry->caption;?></textarea>
				</div>

				<div class="form-actions form-group text-center">
					<button type="submit" class="btn btn-lg btn-success"> <span class="typcn typcn-tick-outline"></span> &nbsp;<?php print locale('save');?> </button>
				</div><div class="clearfix"></div>
			</form>
		</div>
		<div class="col-md-3">
			<div class="group-control">&nbsp;</div>

		</div>
	</div>
</div>