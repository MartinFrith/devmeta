<div class="content-height">
	<div class="center-block row">
		<div class="col-md-12">
			<h3>&nbsp;<span class="typcn typcn-world"></span> <?php echo $entry->title;?></h3>
			<input type="hidden" name="id" value="<?php echo $entry->id;?>">
			<form class="form" method="post" action="/admin/users/update/<?php echo $entry->id;?>">
			
				<div class="form-group">
					<input type="text" name="title" class="form-control input-sm" placeholder="user" value="<?php echo $entry->title;?>" />
				</div>
				<div class="form-group">
					<select class="form-control" name="country_id">
					<?php foreach($options->country as $country):?>
						<option value="<?php echo $country->id;?>"<?php echo $country->id == $entry->country_id?' selected':'';?>><?php echo $country->country;?></option>
					<?php endforeach;?>
					</select>
				</div>				
				<div class="form-group">
					<input type="text" name="username" class="form-control input-sm" placeholder="username" value="<?php echo $entry->username;?>" />
				</div>
				<div class="form-group">
					<input type="email" name="email" class="form-control input-sm" placeholder="email" value="<?php echo $entry->email;?>" />
				</div>

				<div class="form-group">
					<input type="text" name="location" class="form-control input-sm" placeholder="location" value="<?php echo $entry->location;?>" />
				</div>

				<div class="form-actions form-group text-center">
					<button type="submit" class="btn btn-lg btn-success"> <span class="typcn typcn-tick-outline"></span> &nbsp;<?php print locale('save');?> </button>
				</div><div class="clearfix"></div>
			</form>
		</div>
		<div class="col-md-3">
			<div class="group-control">&nbsp;</div>

		</div>
	</div>
</div>