		<div class="group-control">&nbsp;</div>
		<div class="list-group">
			<a href="/admin/posts" class="list-group-item<?php echo segments(2) == 'posts' ? ' active' : '';?>"><span class="typcn typcn-pen"></span> <?php print locale('posts');?> </a>
			<a href="/admin/words" class="list-group-item<?php echo segments(2) == 'words' ? ' active' : '';?>"><span class="typcn typcn-world"></span> <?php print locale('words');?> </a>
			<a href="/admin/projects" class="list-group-item<?php echo segments(2) == 'projects' ? ' active' : '';?>"><span class="typcn typcn-folder-open"></span> <?php print locale('projects');?> </a>
			<a href="/admin/tasks" class="list-group-item<?php echo segments(2) == 'tasks' ? ' active' : '';?>"><span class="typcn typcn-document-text"></span> <?php print locale('tasks');?> </a>
			<a href="/admin/schedules" class="list-group-item<?php echo segments(2) == 'schedules' ? ' active' : '';?>"><span class="typcn typcn-stopwatch"></span> <?php print locale('schedules');?> </a>
			<a href="/admin/calendar" class="list-group-item<?php echo segments(2) == 'calendar' ? ' active' : '';?>"><span class="typcn typcn-calendar"></span> <?php print locale('calendar');?> </a>
			<a href="/admin/payments" class="list-group-item<?php echo segments(2) == 'payments' ? ' active' : '';?>"><span class="typcn typcn-credit-card"></span> <?php print locale('payments');?> </a>
			<a href="/admin/users" class="list-group-item<?php echo segments(2) == 'users' ? ' active' : '';?>"><span class="typcn typcn-user"></span> <?php print locale('users');?> </a>
			<a href="/logout" class="list-group-item<?php echo segments(2) == 'logout' ? ' active' : '';?>"><span class="typcn typcn-key"></span> <?php print locale('logout');?> </a>
		</div>
