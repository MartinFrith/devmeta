<div class="content-height">
	<div class="center-block row">
		<div class="col-sm-3 col-lg-2">
			<?php include SP . 'app/views/admin/sidebar.php';?>
		</div>	
		<div class="col-sm-9 col-lg-10">
			<h3>&nbsp;<span class="typcn typcn-world"></span> <?php print locale('words');?> <a href="/admin/words/create" role="button" class="text-success" title="Create a new word" data-placement="right"><span class="typcn typcn-document-add"></span></a></h3>
			<?php echo messages();?>
		<?php if(count($entries)):?>
			<table class="table">
				<tr>
					<th><?php print locale('key');?></th>
					<th class="text-right"><span class="typcn typcn-cog"></span></th>
				</tr>
			<?php foreach($entries as $word):?>
				<tr>
					<td><span class="text-success"><?php print $word->word_key;?></span><br><?php echo $word->{'word_' . LOCALE};?></td>
					<td class="text-right">
						<form class="nowrap" action="/admin/words/delete/<?php echo $word->id;?>" method="post">
						<button type="submit" class="btn btn-danger" onclick="if(!confirm('Your are about to delete this word, are you sure?')) return false;" title="Delete"><span class="typcn typcn-times-outline"></span></button>
						<a href="/admin/words/<?php echo $word->id;?>" class="btn btn-success" title="Edit"><span class="typcn typcn-edit"></span></a>
						</form>
					</td>
				</tr>
			<?php endforeach;?>
			</table>
			<?php $entries[0]->paginator();?>
		<?php else:?>
				<p> Words is empty! You may create one.</p>
		<?php endif;?>
			<p> <a href="/" target="_blank">Go to Frontpage</a></p>
		</div>
	
	</div>
</div>