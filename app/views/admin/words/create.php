<div class="content-height">
	<div class="center-block row">
		<div class="col-md-12">
			<h3>&nbsp;<span class="typcn typcn-edit"></span> <?php echo locale('word');?></h3>
			<form class="form" method="word" action="/admin/words/update/0">
				<div class="form-group">
					<input type="text" name="word_key" class="form-control input-lg" placeholder="<?php echo locale('word_key');?>" value="" />
				</div>
				<div class="form-actions form-group text-center">
					<button type="submit" class="btn btn-lg btn-success"> <span class="typcn typcn-tick-outline"></span> &nbsp;<?php print locale('save');?> </button>
				</div><div class="clearfix"></div>
			</form>
		</div>
	</div>
</div>