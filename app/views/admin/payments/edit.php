<div class="content-height">
	<div class="center-block row">
		<div class="col-md-12">
			<h3>&nbsp;<span class="typcn typcn-world"></span> <?php echo $entry->id;?></h3>
			<input type="hidden" name="id" value="<?php echo $entry->id;?>">
			<form class="form" method="post" action="/admin/payments/update/<?php echo $entry->id;?>">
				<div class="form-group">
					<input type="text" name="amount" class="form-control input-sm" placeholder="amount" value="<?php echo $entry->amount;?>" />
				</div>
				<div class="form-group">
                    <div class="onoffswitch">
                    	<input type="hidden" name="approved" value="<?php echo $entry->approved;?>">
                        <input type="checkbox" class="onoffswitch-checkbox" id="myonoffswitch"<?php echo $entry->approved?' checked':'';?>>
                        <label class="onoffswitch-label" for="myonoffswitch">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>				
				<div class="form-group">
					<select class="form-control" name="project_id">
					<?php foreach($options->project as $project):?>
						<option value="<?php echo $project->id;?>"<?php echo $project->id == $entry->project_id?' selected':'';?>><?php echo $project->title;?></option>
					<?php endforeach;?>
					</select>
				</div>
				<div class="form-group">
					<select class="form-control" name="currency_id">
					<?php foreach($options->currency as $currency):?>
						<option value="<?php echo $currency->id;?>"<?php echo $currency->id == $entry->currency_id?' selected':'';?>><?php echo $currency->code;?></option>
					<?php endforeach;?>
					</select>
				</div>	
				<div class="form-group">
					<input type="text" name="transferred" class="form-control datetimepicker" placeholder="transferred" value="<?php echo date(config()->dateformat,$entry->transferred);?>" />
				</div>
				<div class="form-group">
					<textarea class="form-control summernote" name="caption" placeholder="<?php print locale('caption');?>"><?php echo $entry->caption;?></textarea>
				</div><div class="clearfix"></div>

				<div class="form-actions form-group text-center">
					<button type="submit" class="btn btn-lg btn-success"> <span class="typcn typcn-tick-outline"></span> &nbsp;<?php print locale('save');?> </button>
				</div><div class="clearfix"></div>
			</form>
		</div>
		<div class="col-md-3">
			<div class="group-control">&nbsp;</div>

		</div>
	</div>
</div>