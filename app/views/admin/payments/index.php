<div class="content-height">
	<div class="center-block row">
		<div class="col-sm-3 col-lg-2">
			<?php include SP . 'app/views/admin/sidebar.php';?>
		</div>	
		<div class="col-sm-9 col-lg-10">
			<h3>&nbsp;<span class="typcn typcn-credit-card"></span> <?php print locale('payments');?> <a href="/admin/payments/create" role="button" class="text-success" title="Create a new payment" data-placement="right"><span class="typcn typcn-document-add"></span></a></h3>
			<?php echo messages();?>
		<?php if(count($entries)):?>
			<table class="table">
				<tr>
					<th><?php print locale('amount');?></th>
					<th><?php print locale('currency');?></th>
					<th><?php print locale('project');?></th>
					<th><?php print locale('client');?></th>
					<th class="text-right"><span class="typcn typcn-cog"></span></th>
				</tr>
			<?php foreach($entries as $payment):?>
				<tr>
					<td><span><?php print $payment->amount;?></span></td>
					<td><span class="text-success"><?php echo count($payment->currency->data) ? $payment->currency->code  : '';?></span></td>
					<td><span class="text-success"><?php echo count($payment->project->data) ? $payment->project->title  : '';?></span></td>
					<td><span class="text-success"><?php echo count($payment->project->user->data) ? $payment->project->user->title  : '';?></span></td>
					<td class="text-right">
						<form class="nowrap" action="/admin/payments/delete/<?php echo $payment->id;?>" method="post">
						<button type="submit" class="btn btn-danger" onclick="if(!confirm('Your are about to delete this payment, are you sure?')) return false;" title="Delete"><span class="typcn typcn-times-outline"></span></button>
						<a href="/admin/payments/<?php echo $payment->id;?>" class="btn btn-success" title="Edit"><span class="typcn typcn-edit"></span></a>
						</form>
					</td>
				</tr>
			<?php endforeach;?>
			</table>
			<?php $entries[0]->paginator();?>
		<?php else:?>
				<p> Words is empty! You may create one.</p>
		<?php endif;?>
		</div>
	</div>
</div>