<div class="content-height">
	<div class="center-block row">
		<div class="col-sm-3 col-lg-2">
			<?php include SP . 'app/views/admin/sidebar.php';?>
		</div>	
		<div class="col-sm-9 col-lg-10">
			<h3>&nbsp;<span class="typcn typcn-spanner-outline"></span> <?php print locale('milestones');?> <a href="/admin/milestones/create" role="button" class="text-success" title="Create a new milestone" data-placement="right"><span class="typcn typcn-document-add"></span></a></h3>
			<?php echo messages();?>

		<?php if(count($entries)):?>
			<table class="table">
				<tr>
					<th width="20%"><?php print locale('project');?></th>
					<th><?php print locale('milestones');?></th>
					<!--th class="text-right"><span class="typcn typcn-cog"></span></th-->
				</tr>
			<?php foreach($entries as $project):?>
				<tr>
					<td><span class="text-success"><span class="typcn typcn-folder-open"></span> <?php print $project->title;?></span></td>
					<td>&nbsp;</td>
				</tr>
				<?php foreach($project->milestones() as $milestone):;?>
				<tr>
					<td>&nbsp;</td>
					<td><span class="text-success"><span class="typcn typcn-document-text"></span> <a href="/admin/milestones/<?php print $milestone->id;?>"><?php print $milestone->milestone;?></a></span></td>
				</tr>
				<?php endforeach;?>
			<?php endforeach;?>
			</table>
			<?php $entries[0]->paginator();?>
		<?php endif;?>

		</div>
	</div>
</div>