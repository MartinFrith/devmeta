<div class="content-height">
	<div class="center-block row">
		<div class="col-md-12">
			<h3>&nbsp;<span class="typcn typcn-world"></span> <?php echo locale('schedule');?></h3>
			<form class="form" method="post" action="/admin/schedules/update/0">
				<div class="form-group">
					<select class="form-control" name="task_id">
					<?php foreach($options->task as $task):$task->milestone->load();?>
						<option value="<?php echo $task->id;?>"><?php echo count($task->milestone->data) ? $task->milestone->project->title : '[Sin proyecto]';?> - <?php echo $task->task;?></option>
					<?php endforeach;?>
					</select>
				</div>	
				<div class="form-actions form-group text-center">
					<button type="submit" class="btn btn-lg btn-success"> <span class="typcn typcn-tick-outline"></span> &nbsp;<?php print locale('save');?> </button>
				</div><div class="clearfix"></div>
			</form>
		</div>
	</div>
</div>