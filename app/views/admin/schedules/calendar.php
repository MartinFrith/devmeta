<div class="content-height">
	<div class="center-block row"> 
		<div class="col-sm-3 col-lg-2">
			<?php include SP . 'app/views/admin/sidebar.php';?>
		</div>		
		<div class="col-sm-9 col-lg-10">
			<div id='calendar' data-url='/admin/schedules/calendar'></div>
		</div>		
	</div>
</div>