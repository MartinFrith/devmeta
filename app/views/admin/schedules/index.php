<div class="content-height">
	<div class="center-block row">
		<div class="col-sm-3 col-lg-2">
			<?php include SP . 'app/views/admin/sidebar.php';?>
		</div>	
		<div class="col-sm-9 col-lg-10">
			<h3>&nbsp;<span class="typcn typcn-stopwatch"></span> <?php print locale('schedules');?> <a href="/admin/schedules/create" role="button" class="text-success" title="Create a new schedule" data-placement="right"><span class="typcn typcn-document-add"></span></a></h3>
			<?php echo messages();?>
		<?php if(count($entries)):?>
			<table class="table">
				<tr>
					<th><?php print locale('task');?></th>
					<th><?php print locale('project');?></th>
					<th><?php print locale('started');?></th>
					<th><?php print locale('ended');?></th>
					<th class="text-right"><span class="typcn typcn-cog"></span></th>
				</tr>
			<?php foreach($entries as $schedule):?>
				<tr>
					<td><span><?php print $schedule->task->task;?></span></td>
					<td><span><?php print count($schedule->task->milestone->data) ? $schedule->task->milestone->project->title : '?';?></span></td>
					<td><span class="text-success"><?php echo parse_date($schedule->started);?></span></td>
					<td><span class="text-success"><?php echo parse_date($schedule->ended);?></span></td>
					<td class="text-right">
						<form class="nowrap" action="/admin/schedules/delete/<?php echo $schedule->id;?>" method="post">
						<button type="submit" class="btn btn-danger" onclick="if(!confirm('Your are about to delete this schedule, are you sure?')) return false;" title="Delete"><span class="typcn typcn-times-outline"></span></button>
						<a href="/admin/schedules/<?php echo $schedule->id;?>" class="btn btn-success" title="Edit"><span class="typcn typcn-edit"></span></a>
						</form>
					</td>
				</tr>
			<?php endforeach;?>
			</table>
			<?php $entries[0]->paginator();?>
		<?php endif;?>
		</div>
	
	</div>
</div>