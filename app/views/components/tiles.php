        <?php if( ! empty($tiles)):?>
            <!-- blog-info -->
            <div class="blog-info" id="blog">
                <div class="wrap">
                    <h3><?php print locale($tile_title);?><span> </span></h3>
                </div>
                <!-- start-img-cursual -->
                <div class="wrap">
                <!-- start content_tiler -->
                    <div id="" class="">
                    <?php foreach($tiles as $tile):?>
                        <div class="item">
                            <div class="item-wrap">
                                <div class="blog-post-info">
                                    <div class="blog-post-info-left">
                                        <span><?php echo date('M',$tile->created);?><label> <?php echo date('d',$tile->created);?></label></span>
                                        <a href="/<?php print $tile->slug;?>#disqus_thread"><small> </small></a>
                                    </div>
                                    <div class="blog-post-info-right"> 
                                        <a class="more-info" href="/<?php print $tile->slug;?>">                                       
                                            <img class="" src="/upload/posts/sd/<?php echo (count($tile->files()) AND file_exists('upload/posts/sd/' . $tile->files()[0]->name)) ? $tile->files()[0]->name : 'default.png';?>" alt="<?php print $tile->{'title_' . LOCALE};?>">
                                        </a>
                                        <h4><a href="#"><?php print $tile->{'title_' . LOCALE};?></a></h4>                                          
                                        <ul class="stats">
                                            <li><span>post by</span><a href="#">Themecurve</a></li>
                                            <li><a href="#">Street</a></li>
                                            <li><a href="#disqus_thread">5 Comments</a></li>
                                            <div class="clear"> </div>
                                        </ul>
                                        <p class="post-text">
                                            <?php print $tile->{'caption_' . LOCALE};?>
                                            <a class="more-info" href="/<?php print $tile->slug;?>"> <span> </span><?php print locale('continue-reading');?></a>
                                        </p>
                                    </div>
                                    <div class="clear"> </div>
                                </div>
                            </div>
                            <div class="clear"> </div>
                        </div>
                    <?php endforeach;?>

                    </div>
                    <!-- Owl Carousel Assets -->
                    <!-- Prettify -->
                    <!-- script src="/js/owl.carousel.js"></script>
                    <script>
                    $(document).ready(function() {
                      $("#owl-demo").owlCarousel({
                        items :1,
                        lazyLoad : true,
                        autoPlay : true,
                        navigation : true,
                        navigationText : ["",""],
                        rewindNav : false,
                        scrollPerPage : false,
                        pagination : false,
                        paginationNumbers: false,
                      });                
                    });
                    </script-->
                    <!-- //Owl Carousel Assets -->
                    <!-- -//End-img-cursual- -->
                </div>
            </div>
            <!-- //blog-info -->
        <?php endif;?>