        <?php if( ! empty($slides)):?>
            <!-- blog-info -->
            <div class="blog-info" id="blog">
                <div class="wrap">
                    <h3><?php print locale($slide_title);?><span> </span></h3>
                </div>
                <!-- start-img-cursual -->
                <div class="wrap">
                <!-- start content_slider -->
                    <div id="owl-demo" class="owl-carousel">
                    <?php foreach($slides as $slide):?>
                        <div class="item">
                            <div class="item-wrap">
                                <img class="lazyOwl post-pic" data-src="/upload/posts/sd/<?php echo (count($slide->files()) AND file_exists('upload/posts/sd/' . $slide->files()[0]->name)) ? $slide->files()[0]->name : 'default.png';?>" alt="<?php print $slide->{'title_' . LOCALE};?>">
                                <div class="blog-post-info">
                                    <div class="blog-post-info-left">
                                        <span><?php echo date('M',$slide->created);?><label> <?php echo date('d',$slide->created);?></label></span>
                                        <a href="/<?php print $slide->slug;?>#disqus_thread"><small> </small></a>
                                    </div>
                                    <div class="blog-post-info-right">
                                        <h4><a href="/<?php print $slide->slug;?>"><?php print $slide->{'title_' . LOCALE};?></a></h4>
                                        <ul class="stats">
                                            <li><span>post by</span><a href="#">Themecurve</a></li>
                                            <li><a href="#">Street</a></li>
                                            <li><a href="#disqus_thread">5 Comments</a></li>
                                            <div class="clear"> </div>
                                        </ul>
                                        <p class="post-text">
                                            <?php print $slide->{'caption_' . LOCALE};?>
                                            <a class="more-info" href="/<?php print $slide->slug;?>"> <span> </span><?php print locale('continue-reading');?></a>
                                        </p>
                                    </div>
                                    <div class="clear"> </div>
                                </div>
                            </div>
                            <!--div class="item-right">
                                <p class="quot">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum delenti atque corrupti </p>
                                <ul>
                                    <li><a href="#">Davinski Mortge</a></li>
                                    <li><span> </span></li>
                                    <div class="clear"> </div>
                                </ul>
                            </div-->
                            <div class="clear"> </div>
                        </div>
                    <?php endforeach;?>

                    </div>
                    <!-- Owl Carousel Assets -->
                    <!-- Prettify -->
                    <script src="/js/owl.carousel.js"></script>
                    <script>
                    $(document).ready(function() {
                      $("#owl-demo").owlCarousel({
                        items :1,
                        lazyLoad : true,
                        autoPlay : true,
                        navigation : true,
                        navigationText : ["",""],
                        rewindNav : false,
                        scrollPerPage : false,
                        pagination : false,
                        paginationNumbers: false,
                      });                
                    });
                    </script>
                    <!-- //Owl Carousel Assets -->
                    <!-- -//End-img-cursual- -->
                </div>
            </div>
            <!-- //blog-info -->
        <?php endif;?>