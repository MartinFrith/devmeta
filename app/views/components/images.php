        <?php if( ! empty($images)):?>
            <!-- blog-info -->
            <div class="blog-info" id="blog">
                <div class="wrap">
                    <h3><?php print locale($image_title);?><span> </span></h3>
                </div>
                <!-- start-img-cursual -->
                <div class="wrap">
                <!-- start content_slider -->
                    <div id="owl-img" class="owl-carousel">
                    <?php foreach($images as $image):?>
                        <div class="item">
                            <div class="item-wrap">
                                <img class="lazyOwl post-pic" data-src="/upload/posts/sd/<?php echo ($image AND file_exists('upload/posts/sd/' . $image->name)) ? $image->name : 'default.png';?>" alt="<?php print $image->title;?>">
                            </div>
                            <div class="clear"> </div>
                        </div>
                    <?php endforeach;?>

                    </div>
                    <!-- Owl Carousel Assets -->
                    <!-- Prettify -->
                    <script src="/js/owl.carousel.js"></script>
                    <script>
                    $(document).ready(function() {
                      $("#owl-img").owlCarousel({
                        items :1,
                        lazyLoad : true,
                        autoPlay : true,
                        navigation : true,
                        navigationText : ["",""],
                        rewindNav : false,
                        scrollPerPage : false,
                        pagination : false,
                        paginationNumbers: false,
                      });                
                    });
                    </script>
                    <!-- //Owl Carousel Assets -->
                    <!-- -//End-img-cursual- -->
                </div>
            </div>
            <!-- //blog-info -->
        <?php endif;?>