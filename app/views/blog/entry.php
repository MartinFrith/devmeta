            <!-- blog-info -->
            <div class="blog-info" id="blog">
                <!-- -start-img-cursual- -->
                <div class="wrap">
                <!-- start content_slider -->
                    <div id="owl-demo2" class="owl-carousel2">
                        <div class="item">
                            <div class="item-center">
                                <!--img class="lazyOwl post-pic" data-src="/upload/posts/sd/<?php echo (count($entry->files()) AND file_exists('upload/posts/sd/' . $entry->files()[0]->name)) ? $entry->files()[0]->name : 'default.png';?>" data-mfp-src="<?php echo (count($entry->files()) AND file_exists('upload/posts/sd/' . $entry->files()[0]->name)) ? $entry->files()[0]->name : 'default.png';?>" alt="<?php echo $entry->{'title_' . LOCALE};?>"-->
                                <div class="blog-post-info">
                                    <div class="blog-post-info-left">
                                        <span><?php echo date('M',$entry->created);?><label> <?php echo date('d',$entry->created);?></label></span>
                                        <a href="#disqus_thread"><small> </small></a>
                                    </div>
                                    <div class="blog-post-info-right">
                                        <h3><?php echo $entry->{'title_' . LOCALE};?></h3>
                                        <ul class="stats">
                                            <li><span>post by</span><a href="#">Admin</a> on</li>
                                            <?php foreach($entry->tags() as $tag):?>
                                                <li><a href="/tag/<?php echo $tag->tag;?>"><?php echo $tag->tag;?></a></li>
                                            <?php endforeach;?>
                                            <li><a href="#">5 Comments</a></li>
                                            <div class="clear"> </div>
                                        </ul>
                                        <p class="post-text">
                                            <?php echo $entry->{'content_' . LOCALE};?>
                                        </p>
                                    </div>

            <?php 
                $images = $entry->files(); 
                $image_title = "";
                include SP . 'app/views/components/images.php';
            ?>


            <?php 
                $slides = $related; 
                $slide_title = locale('related');
                include SP . 'app/views/components/slides.php';
            ?>

                                    <div class="clear"> </div>
                                </div>
                                <div id="disqus_thread"></div>
                            </div>
                            <div class="clear"> </div>
                        </div>
                    </div>
                </div>
            </div>



<script>
    /**
    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
    /*
    var disqus_config = function () {
    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    */
    (function() { // DON'T EDIT BELOW THIS LINE
    var d = document, s = d.createElement('script');
    s.src = 'https://devmeta.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<script id="dsq-count-scr" src="//devmeta.disqus.com/count.js" async></script>

