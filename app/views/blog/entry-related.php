        <!--Sub header-->
        <section class="obliqueCut lightSection imageBackground img5Bkg paddingTop50"></section>
        <!--END Sub header-->

        <!--Blog section-->
        <section id="sBlog" class="obliqueCut lightSection separatorTop paddingBottom200">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="separator"  viewBox="0 0 100 100" preserveAspectRatio="none">
                <path d="M0 100 L100 0 L100 100 Z"></path>
            </svg>
            <div class="sectionIcon  icon_ribbon_alt"></div>
            <div class="container">
                <!-- START TITLE AND DESCRIPTION -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="sectionInfo row">
                            <h2 class="sectionTitle col-sm-4"><span class="underline">Our</span> blog.</h2>
                            <h2 class="sectionDescription col-sm-8"></h2>
                        </div>
                    </div>
                </div>
                <!-- END TITLE AND DESCRIPTION -->

                <!-- START BLOG CONTENT -->
                <div class="row ">
                    <!--MAIN COLUMN-->
                    <div class="col-sm-9">
                        <div class="blog_latest style3">
                            <!-- START ARTICLE -->
                            <div class="article_container">
                                <article>
                                    <div class="mediaContainer scaleRotateImg" data-appear="fade-in" data-appear-direction="top">
                                       <img src="/upload/posts/sd/<?php echo (count($entry->files()) AND file_exists('upload/posts/sd/' . $entry->files()[0]->name)) ? $entry->files()[0]->name : 'default.png';?>" data-mfp-src="<?php echo (count($entry->files()) AND file_exists('upload/posts/sd/' . $entry->files()[0]->name)) ? $entry->files()[0]->name : 'default.png';?>" alt="<?php echo $entry->{'title_' . LOCALE};?>" class="animate img-responsive magPopupImg" />
                                    </div>
                                    <div class="meta" data-appear="fade-in" data-appear-direction="left">
                                        <div class="article_type icon_images"></div>
                                        <div class="article_date"><?php echo timespan($entry->created);?></div>
                                    </div>
                                    <div class="article_content" data-appear="fade-in" data-appear-direction="right">
                                        <h3 class="article_title"><a href="#"><?php echo $entry->{'title_' . LOCALE};?></a></h3>
                                        <!--div class="article_meta">
                                            <span class="article_author">by <a href="#">Admin</a></span>
                                            <span class="article_categories">Categories: <a href="#">Photoshop</a>, <a href="#">Website Design</a>, <a href="#">Development</a></span>
                                        </div-->
                                        <?php echo $entry->{'content_' . LOCALE};?>
                                    </div>
                                </article>

                                <!--SOCIAL SHARE AND TAGS-->
                                <div class="article_bottom">
                                    <div class="socialShare" data-appear="fade-in" data-appear-direction="left">
                                        <span class="titleColor">Share this post:</span>
                                        <a href="#" class="social_facebook tooltip_top" title="Facebook"></a>
                                        <a href="#" class="social_twitter tooltip_top" title="Twitter"></a>
                                        <a href="#" class="social_googleplus tooltip_top" title="Google+"></a>
                                        <a href="#" class="social_linkedin tooltip_top" title="Linkedin"></a>
                                        <a href="#" class="social_tumbleupon tooltip_top" title="Stumbleupon"></a>
                                    </div>
                                    <span class="article_tags" data-appear="fade-in" data-appear-direction="right">
                                        <span class="titleColor">Tags:</span>

                                        <?php foreach($entry->tags() as $tag):?>
                                            <a href="/tag/<?php echo $tag->tag;?>"><?php echo $tag->tag;?></a>
                                        <?php endforeach;?>
                                    </span>
                                </div>
                                <!-- END SOCIAL SHARE AND TAGS-->
                            </div>
                            <!-- END ARTICLE -->
                        </div>
                    </div>

                    <!--START SIDEBAR-->
                    <aside class="col-sm-3">

                        <!--SEARCH WIDGET-->
                        <div class="widget">
                            <div class="widget-search">
                                <form role="search" method="get" class="search-form" action="search">
                                    <input type="search" class="search-field" placeholder="Search …" value="" name="s" title="Search for:" />
                                    <input type="submit" class="search-submit icon_search" value="&#x55;" />
                                </form>
                            </div>
                        </div>
                        <!--END SEARCH WIDGET-->
                    <?php if(!empty($related)):?>
                        <!-- RECENT POSTS WIDGET-->
                        <div class="widget">
                            <h2 class="widget-title underline"><?php print locale('blog-related');?></h2>
                            <ul class="widget-recentPosts">
                            <?php foreach($related as $post):?>
                                <li>
                                    <h4 class="postTitle titleColor"><a href="/<?php echo $post->slug;?>"><?php echo $post->{'title_' . LOCALE};?></a></h4>
                                    <div class="postMeta"><?php echo timespan($post->created);?></div>
                                </li>
                            <?php endforeach;?>
                            </ul>
                        </div>
                        <!-- END RECENT POSTS WIDGET-->
                    <?php endif;?>
                        <!-- TAGS WIDGET-->
                        <div class="widget">
                            <h2 class="widget-title underline">Tags</h2>
                            <ul class="widget-tags clearfix">
                            <?php foreach($tags as $tag):?>
                                <li>
                                    <h4><a href="/tag/<?php echo $tag->tag;?>"><?php echo $tag->tag;?></a></h4>
                                </li>
                            <?php endforeach;?>
                            </ul>
                        </div>
                        <!-- END CATEGORIES WIDGET-->
                    </aside>
                    <!--END START SIDEBAR-->
                </div>
            </div>
            <!-- END CONTAINER -->
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="separator"  viewBox="0 0 100 100" preserveAspectRatio="none">
                <path d="M0 100 L100 0 L0 0 Z"></path>
            </svg>
        </section>
        <!--END Blog section-->