            
        <!--Main slider-->
        <section class="mainSliderContainer">
            <div class="mainSlider">
                <ul>
                <?php $icons=['thumbs-up','coffee','watch','heart'];?>
                <?php for($i=0; $i< 4; $i++):?>

                    <!-- SLIDE 1-->
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="0">
                        <!-- MAIN IMAGE -->
                        <img alt="Slide1" src="/images/bg/<?php print $bgs[$i];?>" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" />

                        <!-- SECONDARY IMAGE -->
                        <!--div class="slideImg tp-resizeme customin customout tp-caption"
                            data-x="870" data-y="95" data-speed="1000" data-start="200"
                            data-customin="x:200px;y:200px;"
                            data-customout="x:200px;y:200px;"
                            data-easing="easeOutExpo">
                            <img alt="iPad" src="/assets/images/bg/<?php print $bgs[$i];?>" />
                        </div-->

                        <!-- TEXT LINE 1 -->
                        <div class="bigText tp-resizeme lft ltt tp-caption"
                            data-x="375" data-y="400" data-speed="1000" data-start="600"
                            data-easing="easeOutExpo">
                            <?php print locale('index_title_' . $i);?>
                        </div>

                        <!-- TEXT LINE 2 -->
                        <div class="mediumText tp-resizeme lft ltt tp-caption"
                            data-x="375" data-y="520" data-speed="1000" data-start="800"
                            data-easing="easeOutExpo">
                            <?php print locale('index_caption_' . $i);?>
                        </div>

                        <!-- TEXT LINE 3 -->
                        <!--div class="mediumText imgArrowOffset tp-resizeme lft ltt tp-caption"
                            data-x="450" data-y="625" data-speed="1000" data-start="1000"
                            data-easing="easeOutExpo">
                            <a href="#sFeaturedWork">See what we have done so far.</a>
                        </div-->

                        <!-- ARROW ON TEXT LINE 3 -->
                        <!--div class="slideImgArrow tp-resizeme lfb ltt tp-caption"
                            data-x="375" data-y="620" data-speed="2000" data-start="700"
                            data-easing="Elastic.easeOut">
                            <a href="#sFeaturedWork">
                                <img alt="Go to portfolio" src="/assets/img/arrow_down.png" /></a>
                        </div-->
                    </li>
                <?php endfor;?>
                </ul>
                <div class="Prev animate">
                    <img alt="Previous" src="/assets/img/prev.png" class="img-responsive" />
                </div>
                <div class="Next animate">
                    <img alt="Next" src="/assets/img/next.png" class="img-responsive" />
                </div>
            </div>
        </section>
        <!--END Main slider-->

        <!--Welcome to untitled section-->
        <section id="sAbout" class="obliqueCut lightSection separatorTop">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="separator" viewBox="0 0 100 100" preserveAspectRatio="none">
                <path d="M0 100 L100 0 L100 100 Z"></path>
            </svg>
            <div class="container">
                <!--SECTION TITLE-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="sectionInfo row">
                            <h2 class="sectionTitle col-sm-4"><span class="underline"><?php print locale('what-we-do');?></span></h2>
                        </div>
                    </div>
                </div>
                <!--END SECTION TITLE-->

                <!--SECTION CONTENT-->
                <div class="row">
                <?php $icons=['edit text-success','gift text-danger','mortar-board text-info'];$circles=['success','danger','info'];for($i=1; $i< 4; $i++):?>
                    <div class="col-sm-4">
                        <!-- START FEATURE BOX -->
                        <div class="featureBox text-center clearfix" data-appear="fade-in" data-appear-direction="left">
                            <h1 class="bigText">
                                <span class="typcn typcn-<?php print $icons[$i-1];?> circle-target"></span> 
                                <span class="circle circle-<?php print $circles[$i-1];?>"></span>
                            </h1>
                                <h3><?php print locale('what-we-do-title-' . $i);?></h3>
                            <p><?php print locale('what-we-do-caption-' . $i);?></p>
                        </div>
                    </div>
                <?php endfor;?>
                </div>
                
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="horizontalLinks">
                            <li><a class="arrowLink arrowRight" href="/tag/works">Learn more about us</a></li>
                            <li><a class="arrowLink arrowRight" href="#contact">Work with us</a></li>
                        </ul>
                    </div>
                </div>
                <!--END SECTION CONTENT-->
            </div>
        </section>
        <!--END Welcome section-->

        <!--Featured work section-->
        <section id="sFeaturedWork" class="obliqueCut darkSection separatorTop">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="separator" viewBox="0 0 100 100" preserveAspectRatio="none">
                <path d="M0 100 L100 0 L100 100 Z"></path>
            </svg>
            <!--SECTION ICON-->
            <div class="sectionIcon icon_archive_alt"></div>
            <div class="container">
                <!--SECTION TITLE-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="sectionInfo row">
                            <h2 class="sectionTitle col-sm-4"><span class="underline"><?php echo locale('index-news-title');?></span> <?php echo locale('index-news');?></h2>
                            <h2 class="sectionDescription col-sm-8"><?php echo locale('index-news-caption');?><br /></h2>
                        </div>
                    </div>
                </div>
                <!--END SECTION TITLE-->
            </div>
            <!--SECTION CONTENT-->
            <div class="fwCarouselContainer carouselContainer" data-appear="fade-in">
                <ul class="fwCarousel magPopupUl">
                <?php foreach($works as $post):?>
                    <li class="scaleRotateImg">
                        <img alt="Image" src="/upload/posts/sd/<?php echo (count($post->files()) AND file_exists('upload/posts/sd/' . $post->files()[0]->name)) ? $post->files()[0]->name : 'default.png';?>" data-mfp-src="<?php echo (count($post->files()) AND file_exists('upload/posts/orig/' . $post->files()[0]->name)) ? '/upload/posts/orig/' . $post->files()[0]->name : '/upload/posts/sd/default.png';?>" class="img-responsive animate" /></li>
                <?php endforeach;?>
                </ul>
                <span class="Prev animate">
                    <img alt="Previous" src="/assets/img/prev.png" /></span>
                <span class="Next animate">
                    <img alt="Next" src="/assets/img/next.png" /></span>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center" data-appear="fade-in" data-appear-direction="top">
                        <a class="goLower" href="/tag/works">View full portfolio</a>
                    </div>
                </div>
            </div>
            <!--END SECTION CONTENT-->
        </section>
        <!--END Featured work section-->


        <!--Services we offer section-->
        <section id="sServices" class="obliqueCut darkSection purpleSection separatorTop extraSpacing2 paddingBottom120">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="separator"  viewBox="0 0 100 100" preserveAspectRatio="none">
                <path d="M0 100 L100 0 L100 100 Z"></path>
            </svg>
            <!--SECTION ICON-->
            <div class="sectionIcon icon_cog"></div>
            <div class="container">
                <!--SECTION TITLE-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="sectionInfo row">
                            <h2 class="sectionTitle col-sm-4"><span class="underline">Services</span> we offer.</h2>
                            <h2 class="sectionDescription col-sm-8">We’re passionate about digital technology and taking on new creative challenges.</h2>
                        </div>
                    </div>
                </div>
                <!--END SECTION TITLE-->

                <!--SECTION CONTENT-->
                <div class="row">
                    <div class="col-sm-4">
                        <!-- START FEATURE BOX -->
                        <div class="featureBox clearfix" data-appear="fade-in" data-appear-direction="left">
                            <h3><span class="icon_chat_alt"></span>Consultation</h3>
                            <p>Before we do anything, we like to sit down together and find out more about you. We want you to tell us your story – of your company, your product and your audience. </p>
                        </div>
                        <!-- END FEATURE BOX -->
                    </div>
                    <div class="col-sm-4">
                        <!-- START FEATURE BOX -->
                        <div class="featureBox clearfix" data-appear="fade-in" data-appear-direction="top" data-appear-delay="200">
                            <h3><span class=" icon_toolbox_alt"></span>Creative</h3>
                            <p>We carefully plan each project before any designing or development takes place. Our main focus here is on the user – we have to make sure any designs we come up with are accessible, intuitive and functional. </p>
                        </div>
                        <!-- END FEATURE BOX -->
                    </div>
                    <div class="col-sm-4">
                        <!-- START FEATURE BOX -->
                        <div class="featureBox clearfix" data-appear="fade-in" data-appear-direction="right" data-appear-delay="400">
                            <h3><span class="icon_mug"></span>Coding & Development</h3>
                            <p>This is where our ideas become visible. We’ll collaborate with you on a number of creative routes, producing content and designing iteratively so that you can see our direction at all times.</p>
                        </div>
                        <!-- END FEATURE BOX -->
                    </div>
                </div>
            </div>
        </section>
        <!--END Services we offer section-->

        <!--Blog section-->
        <section id="sBlog" class="obliqueCut lightSection separatorTop  paddingBottom120">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="separator"  viewBox="0 0 100 100" preserveAspectRatio="none">
                <path d="M0 100 L100 0 L100 100 Z"></path>
            </svg>
            <!--SECTION ICON-->
            <div class="sectionIcon  icon_ribbon_alt"></div>
            <div class="container">
                <!--SECTION TITLE-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="sectionInfo row">
                            <h2 class="sectionTitle col-sm-4"><span class="underline"><?php print locale('blog');?></span></h2>
                            <h2 class="sectionDescription col-sm-8"><?php print locale('blog-caption');?></h2>
                        </div>
                    </div>
                </div>
                <!--END SECTION TITLE-->

                <!-- SECTION CONTENT -->
                <div class="row blog_latest blog_masonry">
                    <?php foreach($posts as $post):?>
                    <!-- START ARTICLE -->
                    <div class="col-sm-4 article_container" data-appear="fade-in">
                        <article>
                            <a href="/<?php echo $post->slug;?>" class="scaleRotateImg">
                                <img src="/upload/posts/sd/<?php echo (count($post->files()) AND file_exists('upload/posts/sd/' . $post->files()[0]->name)) ? $post->files()[0]->name : 'default.png';?>" alt="The Made Shop" class="img-responsive animate col-xs-12" />
                            </a>
                            <div class="meta">
                                <div class="article_type icon_images"></div>
                                <div class="article_date"><?php echo timespan($post->created);?></div>
                            </div>
                            <div class="article_content">
                                <h3><a href="/<?php echo $post->slug;?>"><?php echo $post->{'title_' . LOCALE};?></a></h3>
                                <p><?php echo words($post->{'caption_' . LOCALE},15);?></p>
                                <a class="arrowLink arrowRight" href="/<?php echo $post->slug;?>"><?php print locale('continue-reading');?></a>
                            </div>
                        </article>
                    </div>
                    <!-- END ARTICLE -->
                    <?php endforeach;?>

                </div>
                <div class="row">
                    <div class="view_all_articles col-sm-12"><a class="arrowLink arrowRight" href="/blog">view more blog posts</a></div>
                </div>
                <!--END SECTION CONTENT-->
                

            </div>
            <!-- END CONTAINER -->
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="separator" viewBox="0 0 100 100" preserveAspectRatio="none">
                <path d="M0 100 L100 0 L0 0 Z"></path>
            </svg>
        </section>
        <!--END Blog section-->