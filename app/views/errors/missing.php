            <!-- blog-info -->
            <div class="blog-info" id="blog">
                <!-- -start-img-cursual- -->
                <div class="wrap">
                <!-- start content_slider -->
                    <div id="owl-demo2" class="owl-carousel2">
                        <div class="item">
                            <div class="item-center">
                                <div class="blog-post-info">
                                    <div class="blog-post-info-right">
                                        <h4><a href="#">Not found</a></h4>
                                        <p class="post-text">
                                            <p>The following route <em><?php echo PATH;?></em> &nbsp; is not actually handled directly by this application. This is why the server fell back here. We suggest you try one of the links below:</p><ul><li><b>Verify url and typos</b> - The web page you were attempting to view may not exist or may have moved - try <em>checking the web address for typos</em>.</li><li><b>E-mail us</b> - If you followed a link from somewhere, please let us know at <a href='mailto:info@devmeta.net'>info@devmeta.net</a>. Tell us where you came from and what you were looking for, and we'll do our best to fix it.</li></ul><br><br></p>
                                        </p>
                                    </div>
                                    <div class="clear"> </div>
                                </div>
                            </div>
                            <div class="clear"> </div>
                        </div>
                    </div>
                </div>
            </div>

