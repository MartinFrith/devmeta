<!DOCTYPE html>
<html>
<head>
	<title>Devmeta - Clientes</title>
	<meta charset="utf-8"> 
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<link rel="shortcut icon" href="/assets/favicon.ico" rel="icon" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="/min/?g=css.account"> 
	<script type="text/javascript" src="/min/?g=js.account"></script>
</head>
<body>
	<?php include SP . 'app/views/shared/header-account.php';?>
	<div class="container">
		<div class="row content"><?php echo $content;?></div>
	</div>
	<hr>
	<div class="footer text-center">
        <a target="_blank" title="<?php print locale('twitter');?>" data-placement="top" href="https://twitter.com/devmeta_net">
            <span class="typcn typcn-social-twitter"></span>
        </a>
        <a target="_blank" title="<?php print locale('github');?>" data-placement="top" href="https://github.com/devmeta">
            <span class="typcn typcn-social-github"></span>
        </a>
        <a target="_blank" title="<?php print locale('skype');?>" data-placement="top" href="skype:devmeta.net?chat">
            <span class="typcn typcn-social-skype"></span>
        </a>
        <a target="_blank" title="<?php print locale('mail');?>" data-placement="top" href="mailto:martin@devmeta.net">
            <span class="typcn typcn-mail"></span>
        </a>		
		<a href="/">
			<span class="ion ion-leaf"></span> &nbsp; <small>Devmeta <?php echo date('Y');?></small>
		</a> 
	</div>
</body>
</html>