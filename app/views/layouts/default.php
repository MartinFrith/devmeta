<!--
Template Author: W3layouts
Template Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
    <head>
        <title>Devmeta</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--meta-->
        <meta charset="UTF-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <meta name="keywords" content="we devlopment, php framework" />
        <meta name="description" content="We are a web development company, We build solid applications for the web, We implement Awesome ideas into full scale Projects, Giving birth to your idea on the web">

        <link href="/css/theme.css" rel='stylesheet' type='text/css' />
        <link href="/css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="/css/ionicons.min.css" rel="stylesheet" type="text/css" media="all"/>

        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 10); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        </script>
        <script src="/js/jquery.min.js"></script>
        <script src="/js/modernizr.custom.min.js" type="text/javascript"></script>
        <script src="/js/jquery.magnific-popup.js" type="text/javascript"></script>

        <!-- Favicons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/images/favicons/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/images/favicons/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/images/favicons/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="/assets/images/favicons/apple-touch-icon-57-precomposed.png">
        <link href="/favicon.png" rel="shortcut icon">
        <script> var locale = '<?php echo LOCALE;?>';  </script>
    </head>
    <body>
        
        <?php if($_SERVER['REMOTE_ADDR'] != '127.0.0.1') include SP . 'app/views/shared/analytics.php';?>

        <!-- wrap -->

        <div class="top-bannar" id="move-top"<?php if( ! empty($bg)):?> style="background-image: url(<?php print $bg;?>)"<?php endif;?>>
            <div class="top-header">
                <?php include SP . 'app/views/shared/header.php';?>
            </div>
        </div>

        <!-- start-content -->

        <div class="content">
            <?php echo $content;?>
        </div>

        <?php include SP . 'app/views/shared/footer.php';?>
</body>
</html>