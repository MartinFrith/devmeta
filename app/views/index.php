            <div class="wrap">
                <div class="top-grids" id="about">
                    <?php $icons = [
                        'ion-fork-repo color-blue',
                        'ion-film-marker color-purple',
                        'ion-key color-green'
                    ];?>
                    <h3><?php print locale('index_title_0');?>! <span> </span></h3>
                    <div class="top-grid top-frist">
                        <p class="welcome-para"><?php print locale('index_caption_4');?></p>
                        <a href="/tag/works"> <span> </span><?php print locale('learn-more-about-us');?></a>
                    </div>

                <?php for($i=1; $i< 4; $i++):?>
                    <div class="top-grid">
                        <h4><span class="<?php echo $icons[$i-1];?>"> </span> <?php print locale('what-we-do-title-'.$i);?></h4>
                        <p><?php print locale('what-we-do-caption-'.$i);?></p>
                    </div>
                <?php endfor;?>
                    <div class="clear"> </div>
                </div>
            </div>
            <!-- start-works -->
            <div class="works" id="works">
                <div class="portfolio">
                    <div class="wrap">
                        <h2><?php print locale('featured-work');?><span> </span></h2>
                        
                        <div class="portfolio-bottom">
                            <script src="/js/easyResponsiveTabs.js" type="text/javascript"></script>
                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        $('#horizontalTab').easyResponsiveTabs({
                                            type: 'default', //Types: default, vertical, accordion           
                                            width: 'auto', //auto or any width like 600px
                                            fit: true   // 100% fit in a container
                                        });
                                    });
                                    
                                </script>
                                    
                                        <!-- Portfolio Ends Here -->
                            <div class="sap_tabs">
                                <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                                      <ul class="resp-tabs-list">
                                          <li class="resp-tab-item resp-tab-active" aria-controls="tab_item-0" role="tab"><span><?php print locale('all');?></span></li>
                                        <?php $index = 1;foreach($featured_tags as $tab => $slide):?>
                                          <li class="resp-tab-item" aria-controls="tab_item-<?php echo $index;?>" role="tab"><span><?php echo locale($tab);?></span></li>
                                        <?php $index++; endforeach;?>
                                          <div class="clearfix"></div>
                                      </ul>                  
                                    <div class="resp-tabs-container">
                                        <h2 class="resp-accordion resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span><?php print locale('all');?></h2>
                                        <div class="tab-1 resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
                                            <div class="tab_img">
                                            <?php foreach($featured_tags as $tab => $slides):?>
                                                <?php foreach($slides as $slide):?>
                                                <div class="img-top">
                                                    <a href="/<?php echo $slide->slug;?>" title="<?php echo $slide->{'title_' . LOCALE};?>">
                                                        <img src="/upload/posts/sd/<?php echo (count($slide->files()) AND file_exists('upload/posts/sd/' . $slide->files()[0]->name)) ? $slide->files()[0]->name : 'default.png';?>" class="img-responsive">
                                                    </a>
                                                </div>
                                                <?php endforeach;?>
                                            <?php endforeach;?>
                                                <div class="clearfix"></div>    
                                            </div>                                                                                 
                                        </div>
                                    <?php $index = 1; foreach($featured_tags as $tab => $slides):?>
                                        <h2 class="resp-accordion" role="tab" aria-controls="tab_item-<?php echo $index;?>"><span class="resp-arrow"></span><?php echo locale($tab);?></h2>
                                        <div class="tab-<?php echo $index;?> resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-<?php echo $index;?>">
                                            <div class="tab_img">
                                    
                                            <?php foreach($slides as $slide):?>
                                        
                                                <div class="img-top" title="<?php echo $slide->{'title_' . LOCALE};?>">
                                                    <a href="/<?php echo $slide->slug;?>" title="<?php echo $slide->{'title_' . LOCALE};?>">
                                                        <img src="/upload/posts/sd/<?php echo (count($slide->files()) AND file_exists('upload/posts/sd/' . $slide->files()[0]->name)) ? $slide->files()[0]->name : 'default.png';?>" class="img-responsive">
                                                    </a>
                                                </div>
                                            <?php endforeach;?>
                                            </div>
                                            <div class="clearfix"></div>    
                                        </div>                                   
                                        <?php $index++; endforeach;?>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <!--portfolio end here-->
            </div>

            <?php 
                $slides = $latest; 
                $slide_title = 'latest-from-the-blog';
                include SP . 'app/views/components/slides.php';
            ?>

            <!-- people-says- -->
            <!-- -------------start-testmonials----------- -->
            <div class="test-monials-grids">
                <div class="wrap">
                    <div class="examples" id="textslider">
                        <div class="slider">
                            <div class="slider-mask-wrap">
                                <div class="slider-mask">
                                    <ul class="slider-target instance-1" style="position: relative; left: -3510px; width: 5850px;">
                                    <?php $tesimonials_size = 3; for($i=0;$i<$tesimonials_size;$i++):?>
                                        <li data-slide="<?php echo $i;?>" style="width: 1170px;">
                                            <div class="inner">
                                                <p>"<?php echo locale('testimonials_text_' . ($i+1));?>"</p>
                                            </div>
                                            <div class="test-auther">
                                                <span> </span>
                                                <p><a href="#">- <?php echo locale('testimonials_source_' . ($i+1));?></a></p>
                                            </div>
                                        </li>
                                    <?php endfor;?>
                                    </ul>
                                    <div class="clearit"> </div>
                                </div>
                            </div>
                        </div>
                        <ul id="selector">
                            <?php for($i=0;$i<$tesimonials_size;$i++):?>
                            <li><a href="#" rel="frame_<?php echo $i;?>" class="<?php !$i ? 'current' : '';?>"> </a></li>
                            <?php endfor;?>
                        </ul>
                        <script src="/js/jquery-ui.min.js"></script>
                        <script src="/js/hammer.min.js"></script>
                        <script src="/js/responsiveCarousel.js"></script>
                        <script>
                        /* Okay, everything is loaded. Let's go! (on dom ready) */
                        $(function () {
                            /* a generic product carousel - something that might appear in the body of a e-commerce site. */
                            $('#textslider')
                                .responsiveCarousel({
                                    unitWidth:          'compute',
                                    target:             '#textslider .slider-target',
                                    unitElement:        '#textslider .slider-target > li',
                                    mask:               '#textslider .slider-mask',
                                    arrowLeft:          '#textslider .arrow-left',
                                    arrowRight:         '#textslider .arrow-right',
                                    dragEvents:         Modernizr.touch,
                                    responsiveUnitSize:function () {
                                        return 1;
                                    },
                                    step:-1,
                                    onShift:function (i) {
                                        var $current = $('#selector li a[rel=frame_' + i + ']');
                                        $('#selector li a').removeClass('current');
                                        $current.addClass('current');
                                    }
                                });
                        
                            /* this next part toggles the "auto slide show" option. */
                            $('#toggle-slide-show').on('click', function (ev) {
                                ev.preventDefault();
                                $('#textslider').responsiveCarousel('toggleSlideShow');
                            });
                        
                            /* this lets us jump to a slide */
                            $('#selector a').on('click', function (ev) {
                                ev.preventDefault();
                                var i = /\d/.exec($(this).attr('rel'));
                                $('#textslider').responsiveCarousel('goToSlide', i);
                            });
                        
                        });
                        $(window).on('load',function(){
                            $('.examples').responsiveCarousel('redraw');
                        });
                        </script>
                    </div>
                </div>
            </div>

            <!-- //people-says- -->


            <!-- fea-video -->
            <!--div class="fea-video" id="demo">
                <a class="popup-with-zoom-anim" href="#small-dialog"> </a>
                <p>Beautifully designed with technical excellence and exceptional attention to detail.</p>
            </div>
            <div id="small-dialog" class="mfp-hide">
                <iframe src="//player.vimeo.com/video/38584262" width="100%" height="400" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen> </iframe>
            </div>
            <script>
                    $(document).ready(function() {
                    $('.popup-with-zoom-anim').magnificPopup({
                        type: 'inline',
                        fixedContentPos: false,
                        fixedBgPos: true,
                        overflowY: 'auto',
                        closeBtnInside: true,
                        preloader: false,
                        midClick: true,
                        removalDelay: 300,
                        mainClass: 'my-mfp-zoom-in'
                    });
                                                                                    
                    });
            </script--> 
            <!-- //fea-video -->

